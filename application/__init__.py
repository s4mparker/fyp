import logging, shutil, os, time

# Move any existing files in the logs directory into the archive
for file in [f for f in os.listdir('logs') if os.path.isfile('logs/'+f)] : shutil.move('logs/'+file, 'logs/archive/'+file)

# Create the new logging file
logging.basicConfig(filename=f'logs/app-{time.time_ns()}.log', format='%(levelname)s: %(message)s', level=logging.INFO, filemode='w')

from simulation import Simulation, Parameters, Results
from .widgets import *
from .application import *
