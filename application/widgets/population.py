from . import Panel, DropdownBox, NumberBox
from . import spread, spacing, header_style, text_style, subheader_style
from tkinter import Button, Label
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats._continuous_distns import norm
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

__all__ = ['PopulationPanel']

class PopulationPanel(Panel):

    def __init__(self, master, **kwargs):
        """ Create a new PopulationPanel element 
        
        Parameters:
        master  (tk.Widget) : the element's master
        **kwargs            : other keyword arguments to be passed to the Panel constructor

        """

        # Save a reference to the parameters
        self.parameters = master.parameters

        # Call the superclass' constructor
        super().__init__(master=master, title='', **kwargs)

        Label(master=self, text='Population Configuration', **header_style).grid(row=0, column=0, columnspan=2, **spacing)
        Label(master=self, text='Edit the agents in the simulation. To change the agents that are present, update the parameters file.', **text_style, wraplength=475).grid(row=1, column=0, columnspan=2, **spacing)

        # Create a agent selector
        Label(master=self, text='Selected Agent', **text_style).grid(row=2, column=0, **spacing)
        self.agent = DropdownBox(master=self, on_change=self.refresh)
        self.agent.configure(width=10)
        self.agent.grid(row=2, column=1, **spacing)

        # Create a quantity entry
        Label(master=self, text='Quantity (#)', **text_style).grid(row=3, column=0, **spacing)
        self.quantity = NumberBox(master=self, low=0, high=1000, step=1, on_change=self.changed)
        self.quantity.grid(row=3, column=1, **spacing)

        Label(master=self, text='Vulnerability Distribution', **subheader_style, wraplength=475).grid(row=4, column=0, columnspan=2, **spacing)
        Label(master=self, text='Agent vulnerability (%): 0.0 = healthy, 1.0 = extremely vulnerable', **text_style, wraplength=475).grid(row=5, column=0, columnspan=2, **spacing)

        # Create low & high vulnerability entries
        Label(master=self, text='Vulnerability (mean)', **text_style).grid(row=6, column=0, **spacing)
        Label(master=self, text='Vulnerability (standard deviation)', **text_style).grid(row=6, column=1, **spacing)
        self.mean = NumberBox(master=self, low=0, high=1, step=0.001, on_change=self.changed)
        self.mean.grid(row=7, column=0, **spacing)
        self.std = NumberBox(master=self, low=0, high=1, step=0.001, on_change=self.changed)
        self.std.grid(row=7, column=1, **spacing)

        # Create a minigraph of the agent's vulnerability
        fig, self.ax = plt.subplots(figsize=(3, 3))
        self.graph = FigureCanvasTkAgg(master=self, figure=fig)
        self.graph.get_tk_widget().grid(row=8, column=0, rowspan=4, columnspan=2, **spacing)

        # Create a button to refresh the graph
        Button(master=self, text='Refresh Distribution', command=self.update_graph, **text_style).grid(row=12, column=0, columnspan=2, **spacing)

        # Distribute any spare space
        spread(self, ignore_y=True)

        # Set the widgets' values to the ones contained in the loaded parameters
        self.refresh(initialize=True)

    def refresh(self, initialize=False):
        """ Refresh the values contained in a PopulationPanel's widgets """

        if initialize:
            self.agent.set(self.parameters.get('agents').keys())
        
        info = self.parameters.get_agent(self.agent.get())
        self.quantity.set(info['quantity'])
        self.mean.set(info['mean'])
        self.std.set(info['std'])

        self.update_graph()

    def changed(self):
        """ Update the associated parameter values whenever an option is changed in the PopulationPanel element """

        q = self.quantity.get()
        m = self.mean.get()
        s = self.std.get()
        name = self.agent.get()
        self.parameters.set_agent(name, q, m, s)

    def update_graph(self):
        """ Update the graph showing agent vulnerabilities """

        self.ax.clear()
        self.ax.set_title('Vulnerability Distribution')
        self.ax.set_xlim(left=0, right=1)
        self.ax.set_ylim(bottom=0, top=1)
        self.ax.set_xlabel('Vulnerability')
        self.ax.set_ylabel('Proportion (%)')  
        self.ax.set_ylabel

        mean = self.mean.get()
        std = self.std.get()

        n = 10000
        x = np.clip(np.random.normal(loc=mean, scale=std, size=n), 0, 1)
        counts, bins = np.histogram(x, 100, range=(0, 1))
        counts = counts / n
        bins = bins[:-1] + (0.01 / 2)
        #self.ax.hist(x, bins=100)

        self.ax.plot(bins, counts)

        self.graph.draw_idle()
        self.graph.flush_events()
