from . import Panel
from . import spread, spacing, header_style, text_style
from tkinter import Label, Button

__all__ = ['InstructionsPanel']

class InstructionsPanel(Panel):

    def __init__(self, master, **kwargs):
        """ Create a new InstructionPanel element 
        
        Parameters:
        master  (tk.Widget) : the element's master
        **kwargs            : other keyword arguments to be passed to the Panel constructor

        """

        # Call the superclass' constructor
        super().__init__(master=master, title='', **kwargs)

        # Define instructions
        intro = 'This application was made by Sam Parker (201206398) as part of my submission for Final Year Synoptic Project'
        basic_instructions = "Discrete time agent-based pandemic simulation tool"
        further_instructions = "This tool can be used to model pandemics in user-defined environments. The tabs at the top of the screen can be used to vary the model's parameters and adjust the model's execution. "
        environment_instructions1 = "To create a customized environment, head to File > New Environment to create an empty environment which can then be updated to represent the environment being modelled."
        environment_instructions2 = "To use a newly-created environment, head to File > New Parameters to create an empty parameters file. Using an external editor, edit this file to include the agents you wish to include in the population and set the 'name' and 'path' values to point to your newly created environment. This file can then be loaded into the tool using File > Load > JSON."

        # Create text
        Label(master=self, text='Application Instructions', **header_style).grid(row=0, column=0, **spacing)
        Label(master=self, text=intro, **text_style, wraplength=475).grid(row=1, column=0, **spacing)
        Label(master=self, text='', **text_style, wraplength=475).grid(row=2, column=0, **spacing)
        Label(master=self, text=basic_instructions, **text_style, wraplength=475).grid(row=3, column=0, **spacing)
        Label(master=self, text='', **text_style, wraplength=475).grid(row=4, column=0, **spacing)
        Label(master=self, text=environment_instructions1, **text_style, wraplength=475).grid(row=5, column=0, **spacing)
        Label(master=self, text='', **text_style, wraplength=475).grid(row=6, column=0, **spacing)
        Label(master=self, text=environment_instructions1, **text_style, wraplength=475).grid(row=7, column=0, **spacing)
        Label(master=self, text='', **text_style, wraplength=475).grid(row=8, column=0, **spacing)
        Label(master=self, text=environment_instructions2, **text_style, wraplength=475).grid(row=9, column=0, **spacing)

        # Distribute any available space
        spread(self, ignore_y=True)



