from matplotlib.pyplot import text
from . import Panel, DropdownBox, NumberBox, TickBox
from . import spread, spacing, header_style, text_style
from tkinter import Label, Button

__all__ = ['ControlPanel']

class ControlPanel(Panel):

    def __init__(self, master, **kwargs):
        """ Create a new ControlPanel element 
        
        Parameters:
        master  (tk.Widget) : the element's master
        **kwargs            : other keyword arguments to be passed to the Panel constructor

        """

        # Save a reference to the parameters
        self.parameters = master.parameters

        # Call the superclass' constructor
        super().__init__(master=master, title='Control', **kwargs)

        # Create a models entry
        self.models_label = Label(master=self, text='Concurrent Models', **text_style)
        self.models_label.grid(row=1, column=0, **spacing)
        self.models = NumberBox(master=self, low=1, high=100, step=1, on_change=self.changed, width=2)
        self.models.grid(row=1, column=1, **spacing)

        # Create a execution mode entry
        self.mode_label = Label(master=self, text='Execution Mode', **text_style)
        self.mode_label.grid(row=0, column=0, columnspan=2, **spacing)
        self.mode = DropdownBox(master=self, on_change=self.changed)
        self.mode.configure(width=10)
        self.mode.grid(row=0, column=2, columnspan=2, **spacing)

        # Create a days entry
        self.days_label = Label(master=self, text='Days (#)', **text_style)
        self.days_label.grid(row=1, column=2, **spacing)
        self.days = NumberBox(master=self, low=1, high=1000, step=1, on_change=self.changed)
        self.days.grid(row=1, column=3, **spacing)

        # Create an initial entry
        self.initial_label = Label(master=self, text='Initial Infected', **text_style)
        self.initial_label.grid(row=2, column=0, **spacing)
        self.initial = NumberBox(master=self, low=0, high=1000, step=1, on_change=self.changed)
        self.initial.grid(row=2, column=1, **spacing)

        # Create a spaces entry
        self.spaces_label = Label(master=self, text='Spaces (#)', **text_style)
        self.spaces_label.grid(row=2, column=2, **spacing)
        self.spaces = NumberBox(master=self, low=1, high=1000, step=1, on_change=self.changed)
        self.spaces.grid(row=2, column=3, **spacing)

        # Create reset, step and run buttons
        self.reset_button = Button(master=self, text='Reset Simulation', command=self.reset, state='disabled', **text_style)
        self.reset_button.grid(row=3, column=0, columnspan=2, **spacing)
        self.run_button = Button(master=self, text='Run Simulation', command=self.run, **text_style)
        self.run_button.grid(row=3, column=2, columnspan=2, **spacing)

        # Distribute any spare space
        spread(self, ignore_y=False)

        # Set the widgets' values to the ones contained in the loaded parameters
        self.refresh()

    def refresh(self):
        """ Refresh the values contained in a ControlPanel's widgets """

        mls, m, d, i, s = self.parameters.get('models', 'mode', 'days', 'initial', 'spaces')
        self.mode.set(['serial', 'multiprocessing'], selected=m)
        self.models.set(mls)
        self.days.set(d)
        self.initial.set(i)
        self.spaces.set(s)
        
    def changed(self):
        """ Update the associated parameter values whenever an option is changed in the ControlPanel element """

        self.parameters.set(
            models=self.models.get(),
            mode=self.mode.get(),
            days=self.days.get(),
            initial=self.initial.get(),
            spaces=self.spaces.get()
        )

    def reset(self):
        """ Triggered whenever the reset button is pressed """

        self.reset_button.configure(state='disabled')
        self.run_button.configure(state='normal')

        for element in [self.models, self.mode, self.days, self.initial, self.spaces]:
            element.enable()
        self.models_label.configure(state='normal')
        self.mode_label.configure(state='normal')
        self.days_label.configure(state='normal')
        self.initial_label.configure(state='normal')
        self.spaces_label.configure(state='normal')

        self.master.reset()

    def run(self):
        """ Triggered whenever the run button is pressed """

        self.reset_button.configure(state='normal')
        self.run_button.configure(state='disabled')

        for element in [self.models, self.mode, self.days, self.initial, self.spaces]:
            element.disable()
        self.models_label.configure(state='disabled')
        self.mode_label.configure(state='disabled')
        self.days_label.configure(state='disabled')
        self.initial_label.configure(state='disabled')
        self.spaces_label.configure(state='disabled')

        self.master.run()
        
        
        
