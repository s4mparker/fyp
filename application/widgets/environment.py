from pytest import param
from . import Panel
from . import spread, spacing, header_style, text_style
from tkinter import Button, Label, Text, Scrollbar, VERTICAL, END
import json

__all__ = ['EnvironmentPanel']

class EnvironmentPanel(Panel):

    def __init__(self, master, **kwargs):
        """ Create a new EnvironmentPanel element 
        
        Parameters:
        master  (tk.Widget) : the element's master
        **kwargs            : other keyword arguments to be passed to the Panel constructor

        """

        # Save a reference to the parameters
        self.parameters = master.parameters

        # Call the superclass' constructor
        super().__init__(master=master, title='', **kwargs)

        Label(master=self, text='Environment Configuration', **header_style).grid(row=0, column=0, columnspan=100, **spacing)
        Label(master=self, text='Edit the environment parameters', **text_style, wraplength=475).grid(row=1, column=0, columnspan=100, **spacing)

        # Create a text element
        self.text = Text(master=self)
        self.text.grid(row=2, column=0, columnspan=99, **spacing)

        # Create a scrollbar
        self.scroll = Scrollbar(master=self, orient=VERTICAL)
        self.scroll.grid(row=2, column=99, **spacing)

        # Connect the text element & scrollbar element together
        self.text.configure(yscrollcommand=self.scroll, exportselection=0)
        self.scroll.configure(command=self.text.yview)

        # Create a button to save the parameters
        self.save = Button(master=self, text='Save', command=self.save, height=1, **text_style)
        self.save.grid(row=3, column=0, columnspan=100, **spacing)
        self.save.grid_propagate(0)

        # Distribute any spare space
        spread(self, ignore_y=True)

        # Set the widgets' values to the ones contained in the loaded parameters
        self.refresh()

    def refresh(self):
        """ Refresh the values contained in a EnvironmentPanel's widgets """
        
        self.text.delete('1.0', END)
        parameters = self.parameters.get('env_parameters')
        text = '\n'.join([f'{p}={parameters.get(p, "?")}' for p in parameters])
        self.text.insert('1.0', text)

    def save(self, value=None):
        """ Update the associated environment parameters whenever the EnvironmentPanel element is saved """
        
        self.master.focus()

        try:
            text = str(self.text.get('1.0', END)).replace('\n', '|')
            terms = [t.strip() for t in text.split('|') if len(t) > 0]
            params = {}

            for term in terms:
                if term.count('=') != 1 : raise ValueError('invalid syntax')
                components = [t.strip() for t in term.split('=') if len(t) > 0]
                if len(components) != 2 : raise ValueError('unexpected syntax')
                var = components[0]
                val = components[1]

                try: 
                    val = float(val)
                    val2 = int(val)
                    if val == val2: val = val2
                except : pass
                
                if var in params.keys() : raise ValueError('redefined term')
                else: params[var] = val

            self.parameters.set(**{'env_parameters': params})
            self.master.log('Environment parameters saved')
        except Exception as e:
            self.master.log(f'Failed {e}')