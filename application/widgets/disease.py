from matplotlib.pyplot import text
from . import Panel, DropdownBox, NumberBox
from . import spread, spacing, header_style, text_style, subheader_style
from tkinter import Label

__all__ = ['DiseasePanel']

class DiseasePanel(Panel):

    def __init__(self, master, **kwargs):
        """ Create a new DiseasePanel element 
        
        Parameters:
        master  (tk.Widget) : the element's master
        **kwargs            : other keyword arguments to be passed to the Panel constructor

        """

        # Save a reference to the parameters
        self.parameters = master.parameters

        # Call the superclass' constructor
        super().__init__(master=master, title='', **kwargs)  

        Label(master=self, text='Disease Configuration', **header_style).grid(row=0, column=0, columnspan=2, **spacing)
        Label(master=self, text='Edit the possible stages of infection. To change the stage transitions, update the parameters file.', **text_style, wraplength=475).grid(row=1, column=0, columnspan=2, **spacing)

        # Create a stage selector
        Label(master=self, text='Selected Stage', **text_style).grid(row=2, column=0, **spacing)
        self.stage = DropdownBox(master=self, on_change=self.refresh)
        self.stage.configure(width=10)
        self.stage.grid(row=2, column=1, **spacing)

        Label(master=self, text='Duration Distribution', **subheader_style, wraplength=475).grid(row=3, column=0, columnspan=2, **spacing)
        Label(master=self, text='Stage duration (days)', **text_style, wraplength=475).grid(row=4, column=0, columnspan=2, **spacing)

        # Create length mean & length std entries
        Label(master=self, text='Duration (mean)', **text_style).grid(row=5, column=0, **spacing)
        self.length_mean = NumberBox(master=self, low=0, high=100, step=0.01, on_change=self.changed)
        self.length_mean.grid(row=6, column=0, **spacing)
        Label(master=self, text='Duration (standard deviation)', **text_style).grid(row=5, column=1, **spacing)
        self.length_std = NumberBox(master=self, low=0, high=100, step=0.01, on_change=self.changed)
        self.length_std.grid(row=6, column=1, **spacing)

        Label(master=self, text='Infectivity Distribution', **subheader_style, wraplength=475).grid(row=7, column=0, columnspan=2, **spacing)
        Label(master=self, text='Stage infectivity (%): 0.0 = not infections, 1.0 = every contact is infectious', **text_style, wraplength=475).grid(row=8, column=0, columnspan=2, **spacing)

        # Create infectivtiy mean & infectivtiy std entries
        Label(master=self, text='Infectivity (mean)', **text_style).grid(row=9, column=0, **spacing)
        self.infectivity_mean = NumberBox(master=self, low=0, high=1, step=0.001, on_change=self.changed)
        self.infectivity_mean.grid(row=10, column=0, **spacing)
        Label(master=self, text='Infectivity (standard deviation)', **text_style).grid(row=9, column=1, **spacing)
        self.infectivity_std = NumberBox(master=self, low=0, high=1, step=0.001, on_change=self.changed)
        self.infectivity_std.grid(row=10, column=1, **spacing)

        Label(master=self, text='Fatality Distribution', **subheader_style, wraplength=475).grid(row=11, column=0, columnspan=2, **spacing)
        Label(master=self, text='Stage fatality (%): 0.0 = no fatalities, 1.0 = every infection is fatal', **text_style, wraplength=475).grid(row=12, column=0, columnspan=2, **spacing)

        # Create fatality mean & fatality std entries
        Label(master=self, text='Fatality (mean)', **text_style).grid(row=13, column=0, **spacing)
        self.fatality_mean = NumberBox(master=self, low=0, high=1, step=0.001, on_change=self.changed)
        self.fatality_mean.grid(row=14, column=0, **spacing)
        Label(master=self, text='Fatality (standard deviation)', **text_style).grid(row=13, column=1, **spacing)
        self.fatality_std = NumberBox(master=self, low=0, high=1, step=0.001, on_change=self.changed)
        self.fatality_std.grid(row=14, column=1, **spacing)

        # Distribute any spare space
        spread(self, ignore_y=True)

        # Set the widgets' values to the ones contained in the loaded parameters
        self.refresh(initialize=True)

    def refresh(self, initialize=False):
        """ Refresh the values contained in a DiseasePanel's widgets """

        if initialize:
            self.stage.set(self.parameters.get('stages').keys())

        info = self.parameters.get_stage(self.stage.get())
        self.length_mean.set(info['length']['mean'])
        self.length_std.set(info['length']['std'])
        self.infectivity_mean.set(info['infectivity']['mean'])
        self.infectivity_std.set(info['infectivity']['std'])
        self.fatality_mean.set(info['fatality']['mean'])
        self.fatality_std.set(info['fatality']['std'])

    def changed(self):
        """ Update the associated parameter values whenever an option is changed in the DiseasePanel element """

        lm, lsd = self.length_mean.get(), self.length_std.get()
        im, isd = self.infectivity_mean.get(), self.infectivity_std.get()
        fm, fsd = self.fatality_mean.get(), self.fatality_std.get()
        tag = self.stage.get()
        self.parameters.set_stage(tag, lm, lsd, im, isd, fm, fsd)
