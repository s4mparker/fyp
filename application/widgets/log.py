from . import Panel
from . import spread, spacing
from tkinter import Text, END
from datetime import datetime

__all__ = ['LogPanel']

class LogPanel(Panel):

    def __init__(self, master, **kwargs):
        """ Create a new LogPanel element

        Parameters:
        master      (tk.Widget) : the element's master
        **kwargs                : other keyword arguments to be passed to the Menu constructor
       
        """

        # Call the superclass' constructor
        super().__init__(master=master, title='Log', **kwargs)

        # Create the textbox
        self.text = Text(master=self, state='disabled')
        self.text.grid(row=0, column=0, **spacing)
        self.text.configure(height=2)

        spread(self)

    def add(self, string):
        """ Add a new line to a LogPanel element 
        
        Parameters:
        string  (str) : the string to be added to the element

        """

        # Enable the textbox in order to make changes
        self.text.configure(state='normal')

        # Enter the prefix & string
        prefix = datetime.now().strftime('[%H:%M:%S]')
        self.text.insert(END, f'{prefix}: {string}\n')
        self.text.see(END)

        # Return the textbox to a disabled state
        self.text.configure(state='disabled')

        