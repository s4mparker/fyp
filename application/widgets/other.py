from . import Panel, DropdownBox, NumberBox, TickBox
from . import spread, spacing, header_style, text_style
from tkinter import Label

__all__ = ['OtherPanel']

class OtherPanel(Panel):

    def __init__(self, master, **kwargs):
        """ Create a new OtherPanel element 
        
        Parameters:
        master  (tk.Widget) : the element's master
        **kwargs            : other keyword arguments to be passed to the Panel constructor

        """

        # Save a reference to the parameters
        self.parameters = master.parameters

        # Call the superclass' constructor
        super().__init__(master=master, title='', **kwargs)

        Label(master=self, text='Other Configuration', **header_style).grid(row=0, column=0, columnspan=2, **spacing)
        Label(master=self, text='Update other parameters', **text_style, wraplength=475).grid(row=1, column=0, columnspan=2, **spacing)

        # Create an execution label
        Label(master=self, text='Execution', **text_style).grid(row=2, column=0, columnspan=2, **spacing)

        # Create verbose level entry
        self.verbose_level = DropdownBox(master=self, on_change=self.changed, options=['0', '1', '2', '3'])
        self.verbose_level.grid(row=3, column=0, columnspan=2, **spacing)

        # Create optimisation entries
        self.optimisation = TickBox(master=self, text='Optimisation', on_change=self.changed, **text_style)
        self.optimisation.grid(row=4, column=0, columnspan=2, **spacing)

        # Distribute any spare space
        spread(self, ignore_y=True)

        # Set the widgets' values to the ones contained in the loaded parameters
        self.refresh()

    def refresh(self):
        """ Refresh the values contained in a OtherPanel's widgets """

        v, o = self.parameters.get('verbose', 'optimisation')
        self.verbose_level.set(['0', '1', '2', '3'], selected=v)

    def changed(self):
        """ Update the associated parameter values whenever an option is changed in the OtherPanel element """

        self.parameters.set(
            verbose=int(self.verbose_level.get()),
            optimisation=self.optimisation.get()
        )


