
""" Layout + Presentation """
def spread(frame, ignore_x=False, ignore_y=False):
    gridsize = frame.grid_size()
    if not ignore_x:
        for col in range(gridsize[0]) : frame.columnconfigure(col, weight=1)
    if not ignore_y:
        for row in range(gridsize[1]) : frame.rowconfigure(row, weight=1)
spacing = {'padx': 3, 'pady': 3, 'sticky': 'news'}
header_style = {'font': 'TkDefaultFont 12 bold'}
subheader_style = {'font': 'TkDefaultFont 10 bold'}
text_style = {'font': 'TkDefaultFont 9'}

""" Imports """
from .abstract import *
from .instructions import *
from .control import *
from .dialogs import *
from .disease import *
from .display import *
from .environment import *
from .interventions import *
from .log import *
from .menu import *
from .other import *
from .population import *
