from tkinter import Checkbutton, OptionMenu, Spinbox, Listbox, Scrollbar, LabelFrame, Button, Frame
from tkinter import _setit as setit
from tkinter import END, VERTICAL
from tkinter import BooleanVar, StringVar, IntVar, DoubleVar
from . import spacing, spread

__all__ = ['TickBox', 'DropdownBox', 'NumberBox', 'ListBox', 'Panel', 'PanelButton']

class TickBox(Checkbutton):

    def __init__(self, master, text, on_change=None, value=False, **kwargs):
        """ Create a new TickBox element

        Parameters:
        master      (tk.Widget) : the element's master
        text        (str)       : the element's text
        on_change   (func)      : the command to be ran when the element's value is updated
        value       (bool)      : the element's initial value
        **kwargs                : other keyword arguments to be passed to the CheckButton constructor
       
        """

        # Call the superclass' constructor
        super().__init__(master=master, text=text, command=on_change, **kwargs)

        # Store the element's value
        self.value = BooleanVar(None, value)

        # Final configuration
        self.configure(variable=self.value, offvalue=False, onvalue=True)

    def enable(self):
        """ Enable a TickBox element """

        self.configure(state='normal')

    def disable(self):
        """ Disable a TickBox element """

        self.configure(state='disabled')

    def get(self):
        """ Return the value of a TickBox element 
        
        Returns:
        bool : the element's value

        """

        return self.value.get()

    def set(self, value):
        """ Set the value of a TickBox element 
        
        Parameters:
        value   (bool) : the value which will be assigned to the TickBox element

        """

        self.value.set(value)

class DropdownBox(OptionMenu):

    def __init__(self, master, on_change=None, options=None, **kwargs):
        """ Create a new DropdownBox element 
        
        Parameters:
        master      (tk.Widget) : the element's master
        on_change   (func)      : the command to be ran when the element's value is updated
        options     (list)      : options to be added to the element
        **kwargs                : other keyword arguments to be passed to the OptionMenu constructor

        """

        # Store the element's value
        self.value = StringVar(None, options[0] if options is not None else '')

        # Call the superclass' constructor
        super().__init__(master=master, variable=self.value, value=self.value.get(), **kwargs)

        # Store the command to be ran when the element is changed
        self.command = on_change

        # Add the options to the element
        if options is not None : self.set(options)

    def enable(self):
        """ Enable a DropdownBox element """

        self.configure(state='normal')

    def disable(self):
        """ Disable a DropdownBox element """

        self.configure(state='disabled')

    def get(self):
        """ Return the value of a DropdownBox element 
        
        Returns:
        str : the element's value

        """

        return self.value.get()

    def set(self, options, selected=None):
        """ Set the options available in a DropdownBox element 
        
        Parameters:
        options (list) : the options which should be added to the element

        """

        # Clear the current options
        self['menu'].delete(0, END)

        # Add the new options
        for option in options:
            self['menu'].add_command(label=str(option), command=setit(self.value, str(option), callback=self.change))

        if selected is not None and selected in options : self.value.set(selected)
        else : self.value.set(list(options)[0])

    def change(self, value):
        """ Run the provided command whenever the selected item is changed in the element """

        if self.command is not None : self.command()

class NumberBox(Spinbox):

    def __init__(self, master, low, high, step, on_change=None, **kwargs):
        """ Create a new NumberBox element 
        
        Parameters:
        master      (tk.Widget) : the element's master
        low         (int/float) : the element's lower bound
        high        (int/float) : the element's higher bound
        step        (int/float) : the element's step value
        on_change   (func)      : the command to be ran when the element's value is updated
        **kwargs                : other keyword arguments to be passed to the SpinBox constructor

        """
    
        # Call the superclass' constructor
        super().__init__(master=master, command=self.change, repeatdelay=1, **kwargs)

        # Store the provided command
        self.command = on_change

        # Store the element's value
        if type(step) is int : self.value = IntVar(None, low)
        elif type(step) is float : self.value = DoubleVar(None, low)
        else : raise ValueError('step must be an int / float')

        # Configure the element's value & validation
        vlcmd = (self.register(self.validate), '%P', low, high)
        self.configure(from_=low, to=high, increment=step, textvariable=self.value, validate='all', validatecommand=vlcmd)
        self.bind('<Return>', self.change)

    def validate(self, value, min, max):
        """ Validate a potential change to the element's value 
        
        Parameters:
        value   (str)       : the (potential) future value of the element
        min     (int/float) : the element's minimum value
        max     (int/float) : the element's maximum value

        Returns:
        bool    : whether the given value is valid or not

        """

        result = False
        try: 
            value = float(value)
            result = value >= float(min) and value <= float(max)
        except ValueError : pass
        finally:
            if result : self.configure(foreground='black')
            else : self.configure(foreground='red')

        return True

    def enable(self):
        """ Enable a NumberBox element """

        self.configure(state='normal')

    def disable(self):
        """ Disable a NumberBox element """

        self.configure(state='disabled')

    def get(self):
        """ Return the value of a NumberBox element 
        
        Returns:
        int/float   : the element's value

        """

        return self.value.get()

    def set(self, value):
        """ Set the value of a NumberBox element 
        
        Parameters:
        value   (int/float) : the value which will be assigned to the NumberBox element

        """

        self.value.set(value)

    def change(self, value=None):
        """ Run the provided command whenever the value of the element is changed """

        if self.command is not None : self.command()
        self.master.focus()

class ListBox(Frame):

    def __init__(self, master, on_change=None, options=None, **kwargs):
        """ Create a new ListBox element 
        
        Parameters:
        master      (tk.Widget) : the element's master
        on_change   (func)      : the command to be ran when the element's value is updated
        options     (list)      : options to be added to the element
        **kwargs                : other keyword arguments to be passed to the Frame constructor

        """

        # Call the superclass' constructor
        super().__init__(master=master, **kwargs)

        # Store the command to be ran when the element is changed
        self.command = on_change

        # Store the currently selected value
        self.value = None

        # Create a listbox & a scrollbar
        self.options = Listbox(master=self)
        self.options.grid(row=0, column=0, columnspan=99, sticky='news')
        self.scroll = Scrollbar(master=self, orient=VERTICAL)
        self.scroll.grid(row=0, column=99, sticky='news')

        # Connect the listbox & scrollbar
        self.options.configure(yscrollcommand=self.scroll, exportselection=0)
        self.scroll.configure(command=self.options.yview)

        # Bind the change method to whenever an item in the ListBox is selected
        self.options.bind('<<ListboxSelect>>', self.change)

        # Add the options to the element
        if options is not None : self.set(options)

        # Distribute any spare space
        spread(self)

    def enable(self):
        """ Enable a ListBox element """

        self.options.configure(state='normal')

    def disable(self):
        """ Disable a ListBox element """

        self.options.configure(state='disabled')
    
    def get(self):
        """ Return the value of a ListBox element 
        
        Returns:
        str : the element's value

        """

        return self.value

    def set(self, options):
        """ Set the options available in a ListBox element 
        
        Parameters:
        options (list) : the options which should be added to the element

        """

        # Clear the current options
        self.options.delete(0, END)

        # Add the new options
        for option in options:
            self.options.insert(END, option)

        # Reset the currently selected item
        self.value = None     

    def change(self, value):
        """ Run the provided command whenever the selected item is changed in the element """

        line = self.options.curselection()
        if line == () : self.value = None
        else : self.value = self.options.get(line[0])

        if self.command is not None : self.command()

    def unselect(self):
        """ Unselect the listbox's currently selected item """

        self.value = None
        self.options.select_clear(0, END)

class Panel(LabelFrame):

    def __init__(self, master, title=None, **kwargs):
        """ Create a new Panel element 
        
        Parameters:
        master      (tk.Widget) : the element's master
        title       (str)       : the element's title
        **kwargs                : other keyword arguments to be passed to the LabelFrame constructor

        """

        # Extract the dimensions of the panel if provided
        self.force_x = kwargs.pop('force_x', None)
        self.force_y = kwargs.pop('force_y', None)

        # Call the superclass' constructor
        super().__init__(master=master, text=title, **kwargs, borderwidth=0, highlightthickness=0)

    def enable(self):
        """ Enable a Panel element """

        for child in self.winfo_children() : child.configure(state='normal')

    def disable(self):
        """ Disable a Panel element """

        for child in self.winfo_children() : child.configure(state='disabled')

    def grid(self, **kwargs):

        super().grid(**kwargs)
        if self.force_x is not None or self.force_y is not None:
            self.configure(width=self.force_x, height=self.force_y)
            self.grid_propagate(0)

class PanelButton(Button):

    def __init__(self, master, text, panel, target=(0, 0), **kwargs):
        """ Create a new PanelButton element 
        
        Parameters:
        master      (tk.Widget) : the element's master
        title       (str)       : the element's text
        panel       (Panel)     : the panel associated with the element
        target      (tuple)     : the grid coordinates to be replaced with the associated element
        **kwargs                : other keyword arguments to be passed to the LabelFrame constructor

        """

        # Extract the dimensions of the panel if provided
        self.force_x = kwargs.pop('force_x', None)
        self.force_y = kwargs.pop('force_y', None)

        # Call the superclass' constructor
        super().__init__(master=master, text=text, command=self.clicked, **kwargs)

        # Store the associated panel & coordinates
        self.panel = panel
        self.target = target

    def clicked(self, value=None):
        """ Hide the currently shown panel and show the panel associated with this element """

        widget = self.master.grid_slaves(row=self.target[0], column=self.target[1])
        r, c, rs, cs = 0, 0, 1, 1
        if len(widget) > 0:
            widget = widget[0]
            r, c = widget.grid_info()['row'], widget.grid_info()['column']
            rs, cs = widget.grid_info()['rowspan'], widget.grid_info()['columnspan']
            widget.grid_forget()
        self.panel.grid(row=r, column=c, rowspan=rs, columnspan=cs, **spacing)

    def grid(self, **kwargs):

        super().grid(**kwargs)
        if self.force_x is not None or self.force_y is not None:
            self.configure(width=self.force_x, height=self.force_y)
            self.grid_propagate(0)