from tkinter import Menu
from tkinter.filedialog import askopenfilename, asksaveasfilename
import sys, os
from simulation import Parameters

__all__ = ['MenuBar']

class MenuBar(Menu):

    def __init__(self, master, **kwargs):
        """ Create a new MenuBar element

        Parameters:
        master      (tk.Widget) : the element's master
        **kwargs                : other keyword arguments to be passed to the Menu constructor
       
        """

        # Call the superclass' constructor
        super().__init__(master=master, **kwargs)

        # Set the master's menu to be this instance of MenuBar
        master.configure(menu=self)

        # Save a reference to the simulation's parameters (for easy access)
        self.parameters = master.parameters

        # Create the submenus
        self.add_cascade(label='File', menu=self.create_file_menu())
        self.add_cascade(label='Graphs', menu=self.create_display_menu())

    def create_file_menu(self):
        """ Create a file menu """

        menu = Menu(self, tearoff=0)
        
        import_menu = Menu(menu, tearoff=0)
        import_menu.add_command(label='JSON', command=self.importJSON)
        menu.add_cascade(label='Import', menu=import_menu)
        
        export_menu = Menu(menu, tearoff=0)
        export_menu.add_command(label='JSON', command=self.exportJSON)
        menu.add_cascade(label='Export', menu=export_menu)

        menu.add_command(command=self.newParameters, label='New Parameters')
        menu.add_command(command=self.newEnvironment, label='New Environment')
        menu.add_separator()

        other_menu = Menu(menu, tearoff=0)
        other_menu.add_command(label='Clear Archive', command=self.clear_archive)
        menu.add_cascade(label='Other', menu=other_menu)

        menu.add_command(command=None, label='Help')
        menu.add_command(command=sys.exit, label='Exit')

        return menu

    def create_display_menu(self):

        options = self.master.graph_panel.get_graphs()
        menu = Menu(self, tearoff=0)
        for option in options:
            menu.add_command(label=option, command=lambda var=option: self.master.graph_panel.set_graph(var))
        self.master.graph_panel.set_graph(options[0])
        return menu

    def importJSON(self):
        """ Import new parameters from a JSON file """

        try:
            filename = askopenfilename()
            if filename != () : self.parameters.json_import(filename)
        except : pass

        self.master.disease_panel.refresh(initialize=True)
        self.master.population_panel.refresh(initialize=True)
        self.master.environment_panel.refresh()
        self.master.interventions_panel.refresh()
        self.master.other_panel.refresh()
        self.master.control_panel.refresh()

    def exportJSON(self):
        """ Export the current parameters to a JSON file """

        try:
            filename = asksaveasfilename()
            if filename != () : self.parameters.json_export(filename)
        except : pass

    def clear_archive(self):

        for file in [f for f in os.listdir('logs/archive')] : os.remove(f'logs/archive/{file}')
        print('Clearing archive...')

    def newEnvironment(self):

        filename = asksaveasfilename(defaultextension='.py')
        if filename != ():
            file = open(file=filename, mode='w')
            file.write("from simulation import Environment\n\nclass Placeholder(Environment):\n\n\t\tdef run(self):\n\t\t\n\t\t\tself.random_spaces()\n\t\t\tself.uniform()")
            file.close()

    def newParameters(self):

        filename = asksaveasfilename(defaultextension='.json')
        if filename != ():
            p = Parameters()
            p.set(name='', path='')
            p.json_export(filename=filename)
