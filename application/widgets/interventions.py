from . import Panel, ListBox, TestingDialog, VaccinationDialog, HandWashingDialog, MasksDialog, VentilationDialog, DistancingDialog
from . import spread, spacing, header_style, text_style
from tkinter import Label, Button

__all__ = ['InterventionsPanel']

class InterventionsPanel(Panel):

    def __init__(self, master, **kwargs):
        """ Create a new InterventionsPanel element 
        
        Parameters:
        master  (tk.Widget) : the element's master
        **kwargs            : other keyword arguments to be passed to the Panel constructor

        """

        # Save a reference to the parameters
        self.parameters = master.parameters

        # Call the superclass' constructor
        super().__init__(master=master, title='', **kwargs)

        Label(master=self, text='Disease Configuration', **header_style).grid(row=0, column=0, columnspan=2, **spacing)
        Label(master=self, text='Add new interventions to a simulation', **text_style, wraplength=475).grid(row=1, column=0, columnspan=2, **spacing)

        # Create the buttons to add new interventions
        Button(master=self, text='Add Testing', command=self.add_testing, **text_style).grid(row=2, column=0, columnspan=1, **spacing)
        Button(master=self, text='Add Vaccination', command=self.add_vaccination, **text_style).grid(row=2, column=1, columnspan=1, **spacing)
        Button(master=self, text='Add Handwashing', command=self.add_handwashing, **text_style).grid(row=3, column=0, columnspan=1, **spacing)
        Button(master=self, text='Add Masks', command=self.add_masks, **text_style).grid(row=3, column=1, columnspan=1, **spacing)
        Button(master=self, text='Add Ventilation', command=self.add_ventilation, **text_style).grid(row=4, column=0, columnspan=1, **spacing)
        Button(master=self, text='Add Distancing', command=self.add_distancing, **text_style).grid(row=4, column=1, columnspan=1, **spacing)

        # Add an edit / delete section
        Label(master=self, text='Edit / Delete', **text_style).grid(row=5, column=0, columnspan=2, **spacing)
        Label(master=self, text='Update or delete planned intervention for a simulation.', **text_style, wraplength=475).grid(row=6, column=0, columnspan=2, **spacing)

        self.edit = ListBox(master=self, on_change=self.edit)
        self.edit.grid(row=7, column=0, columnspan=2, **spacing)

        # Distribute any spare space
        spread(self, ignore_y=True)

        # Set the widgets' values to the ones contained in the loaded parameters
        self.refresh()

    def refresh(self):
        """ Refresh the values contained in an InterventionPanel's widgets """

        options = [(i['type'], i['day']) for i in self.parameters.get('interventions')]
        options = sorted(options, key=lambda value: (int(value[1]), str(value[0])))
        string_options = [f'{o[0].capitalize()} {o[1]}' for o in options]
        self.edit.set(string_options)

    def add_testing(self):
        """ Open a new dialog window to prompt the user to enter a new testing intervention """

        dialog = TestingDialog(master=self, mode='add', parameters=self.parameters)
        response = dialog.respond()
        if response is not None : self.master.log(response)

        self.refresh()

    def add_vaccination(self):
        """ Open a new dialog window to prompt the user to enter a new vaccination intervention """

        dialog = VaccinationDialog(master=self, mode='add', parameters=self.parameters)
        response = dialog.respond()
        if response is not None : self.master.log(response)

        self.refresh()

    def add_handwashing(self):
        """ Open a new dialog window to prompt the user to enter a new handwashing intervention """

        dialog = HandWashingDialog(master=self, mode='add', parameters=self.parameters)
        response = dialog.respond()
        if response is not None : self.master.log(response)

        self.refresh()

    def add_masks(self):
        """ Open a new dialog window to prompt the user to enter a new masks intervention """

        dialog = MasksDialog(master=self, mode='add', parameters=self.parameters)
        response = dialog.respond()
        if response is not None : self.master.log(response)

        self.refresh()

    def add_ventilation(self):
        """ Open a new dialog window to prompt the user to enter a new ventilation intervention """

        dialog = VentilationDialog(master=self, mode='add', parameters=self.parameters)
        response = dialog.respond()
        if response is not None : self.master.log(response)

        self.refresh()

    def add_distancing(self):
        """ Open a new dialog window to prompt the user to enter a new distancing intervention """

        dialog = DistancingDialog(master=self, mode='add', parameters=self.parameters)
        response = dialog.respond()
        if response is not None : self.master.log(response)

        self.refresh()
    
    def edit(self):
        """ Prompt the user to edit / delete an existing intervention """

        # Get the currently selected intervention
        selected = self.edit.get()
        if selected is None : return

        # Extract the intervention type & intervention day
        components = selected.split(' ')
        type = components[0].lower()
        day = components[1]

        # Display the appropiate dialog and log the response
        params = {'master': self, 'mode': 'edit', 'parameters': self.parameters, 'selected': int(day)}
        response = None
        if type == 'testing' : response = TestingDialog(**params).respond()
        if type == 'vaccination' : response = VaccinationDialog(**params).respond()
        if type == 'handwashing' : response = HandWashingDialog(**params).respond()
        if type == 'masks' : response = MasksDialog(**params).respond()
        if type == 'ventilation' : response = VentilationDialog(**params).respond()
        if type == 'distancing' : response = DistancingDialog(**params).respond()
        if response is not None : self.master.log(response)

        # Refresh the options in the listbox
        self.edit.unselect()
        self.refresh()


