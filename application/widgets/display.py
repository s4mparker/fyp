from . import Panel
from . import spread, spacing
from simulation import GraphBuilder
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib.pyplot as plt

__all__ = ['GraphPanel']

class GraphPanel(Panel):

    def __init__(self, master, **kwargs):
        """ Create a new GraphPanel element 
        
        Parameters:
        master  (tk.Widget) : the element's master
        **kwargs            : other keyword arguments to be passed to the Panel constructor

        """

        # Call the superclass' constructor
        super().__init__(master=master, title='Results', **kwargs)

        # Create a figure
        self.figure, self.ax = plt.subplots()

        # Create the canvas
        self.canvas = FigureCanvasTkAgg(master=self, figure=self.figure)
        self.canvas.get_tk_widget().grid(row=0, column=0, **spacing)
        
        # Create the connected graph builder
        self.builder = GraphBuilder(ax=self.ax, results=master.simulation.results)

        # Distribute any spare space
        spread(self)

    def refresh(self):

        self.builder.render()
        self.canvas.draw_idle()
        self.canvas.flush_events()

    def set_graph(self, value):
        self.builder.set_option(value)
        self.refresh()

    def get_graphs(self):

        return self.builder.get_options()
