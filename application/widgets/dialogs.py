from . import TickBox, NumberBox
from . import spacing, spread, header_style, text_style
from tkinter import Label, Button, Toplevel

__all__ = ['TestingDialog', 'VaccinationDialog', 'HandWashingDialog', 'MasksDialog', 'VentilationDialog', 'DistancingDialog']

class BasicDialog(Toplevel):

    def __init__(self, master, parameters, title, mode, serve, selected=None):
        """ Create a new BasicDialog window 
        
        Parameters:
        master      (tk.Widget)     : the dialog's master
        parameters  (Parameters)    : the parameters associated with the dialog
        title       (str)           : the dialog's title
        mode        (str)           : the dialog's operating mode
        serve       (str)           : the type of interventions the dialog serves
        selected    (int)           : the day of the intervention selected from the dropdown menu

        """

        # Call the superclass' constructor
        super().__init__(master=master)
        self.title(title)

        # Save a reference to the parameters & selected value
        self.parameters = master.parameters
        if mode == 'add' and selected is not None : self.destroy('recieved a selected day when adding an intervention')
        elif mode == 'edit' and selected is None : self.destroy('missing a selected day in edit mode')
        elif mode == 'edit' and not self.parameters.contains_intervention(serve, selected) : self.destroy('attempting to edit a day which isn\'t present')
        else : self.selected = selected

        # Store the text response
        self.response = None

    def respond(self):
        """ Wait for a response from the window """

        self.wait_window()
        return self.response

    def destroy(self, message=None):
        self.response = message
        super().destroy()

    def cancel(self):
        """ Cancel the current operation """

        self.destroy()

class TestingDialog(BasicDialog):

    def __init__(self, master, mode, parameters, selected=None):
        """ Create a new TestingDialog window 
        
        Parameters:
        master      (tk.Widget)     : the element's master
        mode        (str)           : the dialog's operating mode
        parameters  (Parameters)    : the parameters associated with the element
        selected    (int)           : the day of the intervention selected from the dropdown menu

        """

        # Call the superclass' constructor
        super().__init__(master=master, parameters=parameters, title='Testing', mode=mode, serve='testing', selected=selected)

        # Create a day entry
        Label(master=self, text='Test the population for infection', **text_style, wraplength=475).grid(row=0, column=0, columnspan=2, **spacing)

        Label(master=self, text='Start Day', **text_style).grid(row=1, column=0, **spacing)
        self.day = NumberBox(master=self, low=0, high=1000, step=1, on_change=self.changed)
        self.day.grid(row=1, column=1, **spacing)

        # Creating a testing toggle

        self.enabled = TickBox(master=self, text='Enabled', on_change=self.changed, value=True, **text_style)
        self.enabled.grid(row=2, column=0, columnspan=2, **spacing)

        # Creating an efficiency entry
        self.efficiency_description = Label(master=self, text='Successfulness of the test: 0 = 0%, 1 = 100%', **text_style, wraplength=475)
        self.efficiency_description.grid(row=3, column=0, columnspan=2, **spacing)

        self.efficiency_label = Label(master=self, text='Efficiency', **text_style)
        self.efficiency_label.grid(row=4, column=0, **spacing)
        self.efficiency = NumberBox(master=self, low=0, high=1, step=0.01)
        self.efficiency.grid(row=4, column=1, **spacing)

        # Creating a frequency entry
        self.frequency_description = Label(master=self, text='Frequency of testing: e.g. 3 = every three days', **text_style, wraplength=475)
        self.frequency_description.grid(row=5, column=0, columnspan=2, **spacing)

        self.frequency_label = Label(master=self, text='Frequency', **text_style)
        self.frequency_label.grid(row=6, column=0, **spacing)
        self.frequency = NumberBox(master=self, low=1, high=100, step=1)
        self.frequency.grid(row=6, column=1, **spacing)

        # Creating a coverage entry
        self.coverage_description = Label(master=self, text='Proportion of the population selected for testing: 0 = 0%, 1 = 100%', **text_style, wraplength=475)
        self.coverage_description.grid(row=7, column=0, columnspan=2, **spacing)

        self.coverage_label = Label(master=self, text='Coverage', **text_style)
        self.coverage_label.grid(row=8, column=0, **spacing)
        self.coverage = NumberBox(master=self, low=0, high=1, step=0.01)
        self.coverage.grid(row=8, column=1, **spacing)

        # Creating an isolation entry
        self.isolation_description = Label(master=self, text='Isolation period for positive tests (days)', **text_style, wraplength=475)
        self.isolation_description.grid(row=9, column=0, columnspan=2, **spacing)

        self.isolation_label = Label(master=self, text='Isolation', **text_style)
        self.isolation_label.grid(row=10, column=0, **spacing)
        self.isolation = NumberBox(master=self, low=0, high=100, step=1)
        self.isolation.grid(row=10, column=1, **spacing)

        # Create the appropiate buttons
        if mode == 'add':
            Button(master=self, text='Cancel', command=self.cancel, **text_style).grid(row=11, column=0, **spacing)
            Button(master=self, text='Confirm', command=self.add, **text_style).grid(row=11, column=1, **spacing)
        elif mode == 'edit':
            Button(master=self, text='Cancel', command=self.cancel, **text_style).grid(row=11, column=0, columnspan=2, **spacing)
            Button(master=self, text='Delete', command=self.delete, **text_style).grid(row=12, column=0, **spacing)
            Button(master=self, text='Save', command=self.save, **text_style).grid(row=12, column=1, **spacing)
        else : raise ValueError('unrecognised mode')

        # Setup the window with initial values
        self.initialize()

        # Distribute any remaining space
        spread(self)

    def initialize(self):
        """ Initialize the dialog's values """

        if self.selected is not None:
            i = self.parameters.get_intervention('testing', self.selected)

            self.day.set(i['day'])
            self.efficiency.set(i['parameters']['efficiency'])
            self.frequency.set(i['parameters']['frequency'])
            self.coverage.set(i['parameters']['coverage'])
            self.isolation.set(i['parameters']['isolation'])
            self.enabled.set(i['parameters']['enabled'])

        self.changed()

    def changed(self):
        """ Validate entered values whenever one is changed """

        matches = [i for i in self.parameters.interventions if i['type'] == 'testing' and i['day'] == self.day.get() and i['day'] != self.selected]
        if len(matches) > 0 : self.day.configure(foreground='red')
        else : self.day.configure(foreground='black')

        if self.enabled.get():
            for wid in [self.efficiency_label, self.efficiency_description, self.frequency_label, self.frequency_description, self.coverage_label, self.coverage_description, self.isolation_label, self.isolation_description] : wid.configure(state='normal')
            for wid in [self.efficiency, self.frequency, self.coverage, self.isolation] : wid.enable()
        else:
            for wid in [self.efficiency_label, self.efficiency_description, self.frequency_label, self.frequency_description, self.coverage_label, self.coverage_description, self.isolation_label, self.isolation_description] : wid.configure(state='disabled')
            for wid in [self.efficiency, self.frequency, self.coverage, self.isolation] : wid.disable()

    def add(self):
        """ Add the new intervention """

        try:
            self.parameters.add_intervention(
                type='testing', 
                day=self.day.get(),
                enabled=self.enabled.get(),
                efficiency=self.efficiency.get(),
                frequency=self.frequency.get(),
                coverage=self.coverage.get(),
                isolation=self.isolation.get()
            )
            self.destroy('Intervention added!')
        except Exception as e:
            self.destroy(e)
            
    def delete(self):
        """ Delete the current intervention """

        try:
            self.parameters.delete_intervention(type='testing', day=self.selected)
            self.destroy('Intervention successfully deleted!')
        except Exception as e:
            self.destroy('e')

    def save(self):
        """ Save the current intervention """

        try:
            if self.day.get() == self.selected:
                self.parameters.delete_intervention(type='testing', day=self.selected)
                self.parameters.add_intervention(
                    type='testing', 
                    day=self.day.get(),
                    enabled=self.enabled.get(),
                    efficiency=self.efficiency.get(),
                    frequency=self.frequency.get(),
                    coverage=self.coverage.get(),
                    isolation=self.isolation.get()
                )
            else:
                self.parameters.add_intervention(
                    type='testing', 
                    day=self.day.get(),
                    enabled=self.enabled.get(),
                    efficiency=self.efficiency.get(),
                    frequency=self.frequency.get(),
                    coverage=self.coverage.get(),
                    isolation=self.isolation.get()
                )
                self.parameters.delete_intervention(type='testing', day=self.selected)     
            self.destroy('Intervention successfully saved!')
        except Exception as e:
            self.destroy(e)

class VaccinationDialog(BasicDialog):

    def __init__(self, master, mode, parameters, selected=None):
        """ Create a new Vaccination window 
        
        Parameters:
        master      (tk.Widget)     : the element's master
        mode        (str)           : the dialog's operating mode
        parameters  (Parameters)    : the parameters associated with the element
        selected    (int)           : the day of the intervention selected from the dropdown menu

        """

        # Call the superclass' constructor
        super().__init__(master=master, parameters=parameters, title='Vaccination', mode=mode, serve='vaccination', selected=selected)

        Label(master=self, text='Vaccinate the population against infection', **text_style, wraplength=475).grid(row=0, column=0, columnspan=2, **spacing)

        # Create a day entry
        Label(master=self, text='Start Day', **text_style).grid(row=1, column=0, **spacing)
        self.day = NumberBox(master=self, low=0, high=1000, step=1, on_change=self.changed)
        self.day.grid(row=1, column=1, **spacing)

        # Create an effect entry
        Label(master=self, text='Protection offered by the vaccination: 0 = 0%, 1 = 100%', **text_style, wraplength=475).grid(row=2, column=0, columnspan=2, **spacing)

        Label(master=self, text='Protection', **text_style).grid(row=3, column=0, **spacing)
        self.effect = NumberBox(master=self, low=0, high=1, step=0.001)
        self.effect.grid(row=3, column=1, **spacing)

        # Create the appropiate buttons
        if mode == 'add':
            Button(master=self, text='Cancel', command=self.cancel, **text_style).grid(row=4, column=0, **spacing)
            Button(master=self, text='Confirm', command=self.add, **text_style).grid(row=4, column=1, **spacing)
        elif mode == 'edit':
            Button(master=self, text='Cancel', command=self.cancel, **text_style).grid(row=4, column=0, columnspan=2, **spacing)
            Button(master=self, text='Delete', command=self.delete, **text_style).grid(row=5, column=0, **spacing)
            Button(master=self, text='Save', command=self.save, **text_style).grid(row=5, column=1, **spacing)
        else : raise ValueError('unrecognised mode')

        # Setup the window with initial values
        self.initialize()

        # Distribute any remaining space
        spread(self)

    def initialize(self):
        """ Initialize the dialog's values """

        if self.selected is not None:
            i = self.parameters.get_intervention('vaccination', self.selected)

            self.day.set(i['day'])
            self.effect.set(i['parameters']['effect'])

        self.changed()

    def changed(self):
        """ Validate entered values whenever one is changed """
        
        matches = [i for i in self.parameters.interventions if i['type'] == 'vaccination' and i['day'] == self.day.get() and i['day'] != self.selected]
        if len(matches) > 0 : self.day.configure(foreground='red')
        else : self.day.configure(foreground='black')
    
    def add(self):
        """ Add the new intervention """

        try:
            self.parameters.add_intervention(
                type='vaccination',
                day=self.day.get(),
                effect=self.effect.get()
            )
            self.destroy('Intervention added!')
        except Exception as e:
            self.destroy(e)

    def delete(self):
        """ Delete the current intervention """

        try:
            self.parameters.delete_intervention(type='vaccination', day=self.selected)
            self.destroy('Intervention successfully deleted!')
        except Exception as e:
            self.destroy(e)

    def save(self):
        """ Save the current intervention """

        try:
            if self.day.get() == self.selected:
                self.parameters.delete_intervention(type='vaccination', day=self.selected)
                self.parameters.add_intervention(
                    type='vaccination',
                    day=self.day.get(),
                    effect=self.effect.get()
                )
            else:
                self.parameters.add_intervention(
                    type='vaccination',
                    day=self.day.get(),
                    effect=self.effect.get()
                )
                self.parameters.delete_intervention(type='vaccination', day=self.selected)
            self.destroy('Intervention successfully saved!')
        except Exception as e:
            self.destroy(e)    

class HandWashingDialog(BasicDialog):

    def __init__(self, master, mode, parameters, selected=None):
        """ Create a new HandwashingDialog window 
        
        Parameters:
        master      (tk.Widget)     : the element's master
        mode        (str)           : the dialog's operating mode
        parameters  (Parameters)    : the parameters associated with the element
        selected    (int)           : the day of the intervention selected from the dropdown menu

        """

        # Call the superclass' constructor
        super().__init__(master=master, parameters=parameters, title='Handwashing', mode=mode, serve='handwashing', selected=selected)

        Label(master=self, text='Enforce handwashing measures on the population', **text_style, wraplength=475).grid(row=0, column=0, columnspan=2, **spacing)

        # Create a day entry
        Label(master=self, text='Start Day', **text_style).grid(row=1, column=0, **spacing)
        self.day = NumberBox(master=self, low=0, high=1000, step=1, on_change=self.changed)
        self.day.grid(row=1, column=1, **spacing)

        # Create a mean entry
        Label(master=self, text='Protection offered by the handwashing measures', **text_style, wraplength=475).grid(row=2, column=0, columnspan=2, **spacing)

        Label(master=self, text='Protection (Mean)', **text_style).grid(row=3, column=0, **spacing)
        self.mean = NumberBox(master=self, low=0, high=1, step=0.001)
        self.mean.grid(row=3, column=1, **spacing)
        
        Label(master=self, text='Protection (Standard Deviation)', **text_style).grid(row=4, column=0, **spacing)
        self.std = NumberBox(master=self, low=0, high=1, step=0.001)
        self.std.grid(row=4, column=1, **spacing)

        # Create the appropiate buttons
        if mode == 'add':
            Button(master=self, text='Cancel', command=self.cancel, **text_style).grid(row=5, column=0, **spacing)
            Button(master=self, text='Confirm', command=self.add, **text_style).grid(row=5, column=1, **spacing)
        elif mode == 'edit':
            Button(master=self, text='Cancel', command=self.cancel, **text_style).grid(row=5, column=0, columnspan=2, **spacing)
            Button(master=self, text='Delete', command=self.delete, **text_style).grid(row=6, column=0, **spacing)
            Button(master=self, text='Save', command=self.save, **text_style).grid(row=6, column=1, **spacing)
        else : raise ValueError('unrecognised mode')

        # Setup the window with initial values
        self.initialize()

        # Distribute any remaining space
        spread(self)

    def initialize(self):
        """ Initialize the dialog's values """

        if self.selected is not None:
            i = self.parameters.get_intervention('handwashing', self.selected)

            self.day.set(i['day'])
            self.mean.set(i['parameters']['mean'])
            self.std.set(i['parameters']['std'])

        self.changed()

    def changed(self):
        """ Validate entered values whenever one is changed """
        
        matches = [i for i in self.parameters.interventions if i['type'] == 'handwashing' and i['day'] == self.day.get() and i['day'] != self.selected]
        if len(matches) > 0 : self.day.configure(foreground='red')
        else : self.day.configure(foreground='black')
    
    def add(self):
        """ Add the new intervention """

        try:
            self.parameters.add_intervention(
                type='handwashing',
                day=self.day.get(),
                mean=self.mean.get(),
                std=self.std.get()
            )
            self.destroy('Intervention added!')
        except Exception as e:
            self.destroy(e)

    def delete(self):
        """ Delete the current intervention """

        try:
            self.parameters.delete_intervention(type='handwashing', day=self.selected)
            self.destroy('Intervention successfully deleted!')
        except Exception as e:
            self.destroy(e)

    def save(self):
        """ Save the current intervention """

        try:
            if self.day.get() == self.selected:
                self.parameters.delete_intervention(type='handwashing', day=self.selected)
                self.parameters.add_intervention(
                    type='handwashing',
                    day=self.day.get(),
                    mean=self.mean.get(),
                    std=self.std.get()
                )
            else:
                self.parameters.add_intervention(
                    type='handwashing',
                    day=self.day.get(),
                    mean=self.mean.get(),
                    std=self.std.get()
                )
                self.parameters.delete_intervention(type='handwashing', day=self.selected)
            self.destroy('Intervention successfully saved!')
        except Exception as e:
            self.destroy(e)   

class MasksDialog(BasicDialog):

    def __init__(self, master, mode, parameters, selected=None):
        """ Create a new MasksDialog window 
        
        Parameters:
        master      (tk.Widget)     : the element's master
        mode        (str)           : the dialog's operating mode
        parameters  (Parameters)    : the parameters associated with the element
        selected    (int)           : the day of the intervention selected from the dropdown menu

        """

        # Call the superclass' constructor
        super().__init__(master=master, parameters=parameters, title='Masks', mode=mode, serve='masks', selected=selected)

        Label(master=self, text='Enforce mask wearing measures on the population', **text_style, wraplength=475).grid(row=0, column=0, columnspan=2, **spacing)

        # Create a day entry
        Label(master=self, text='Start Day', **text_style).grid(row=1, column=0, **spacing)
        self.day = NumberBox(master=self, low=0, high=1000, step=1, on_change=self.changed)
        self.day.grid(row=1, column=1, **spacing)

        Label(master=self, text='Protection offered by the mask wearing measures', **text_style, wraplength=475).grid(row=2, column=0, columnspan=2, **spacing)

        # Create a mean entry
        Label(master=self, text='Protection (Mean)', **text_style).grid(row=3, column=0, **spacing)
        self.mean = NumberBox(master=self, low=0, high=1, step=0.001)
        self.mean.grid(row=3, column=1, **spacing)

        # Create a standard deviation entry
        Label(master=self, text='Protection (Standard Deviation)', **text_style).grid(row=4, column=0, **spacing)
        self.std = NumberBox(master=self, low=0, high=1, step=0.001)
        self.std.grid(row=4, column=1, **spacing)

        # Create the appropiate buttons
        if mode == 'add':
            Button(master=self, text='Cancel', command=self.cancel, **text_style).grid(row=5, column=0, **spacing)
            Button(master=self, text='Confirm', command=self.add, **text_style).grid(row=5, column=1, **spacing)
        elif mode == 'edit':
            Button(master=self, text='Cancel', command=self.cancel, **text_style).grid(row=5, column=0, columnspan=2, **spacing)
            Button(master=self, text='Delete', command=self.delete, **text_style).grid(row=6, column=0, **spacing)
            Button(master=self, text='Save', command=self.save, **text_style).grid(row=6, column=1, **spacing)
        else : raise ValueError('unrecognised mode')

        # Setup the window with initial values
        self.initialize()

        # Distribute any remaining space
        spread(self)

    def initialize(self):
        """ Initialize the dialog's values """

        if self.selected is not None:
            i = self.parameters.get_intervention('masks', self.selected)

            self.day.set(i['day'])
            self.mean.set(i['parameters']['mean'])
            self.std.set(i['parameters']['std'])

        self.changed()

    def changed(self):
        """ Validate entered values whenever one is changed """
        
        matches = [i for i in self.parameters.interventions if i['type'] == 'masks' and i['day'] == self.day.get() and i['day'] != self.selected]
        if len(matches) > 0 : self.day.configure(foreground='red')
        else : self.day.configure(foreground='black')
    
    def add(self):
        """ Add the new intervention """

        try:
            self.parameters.add_intervention(
                type='masks',
                day=self.day.get(),
                mean=self.mean.get(),
                std=self.std.get()
            )
            self.destroy('Intervention added!')
        except Exception as e:
            self.destroy(e)

    def delete(self):
        """ Delete the current intervention """

        try:
            self.parameters.delete_intervention(type='masks', day=self.selected)
            self.destroy('Intervention successfully deleted!')
        except Exception as e:
            self.destroy(e)

    def save(self):
        """ Save the current intervention """

        try:
            if self.day.get() == self.selected:
                self.parameters.delete_intervention(type='masks', day=self.selected)
                self.parameters.add_intervention(
                    type='masks',
                    day=self.day.get(),
                    mean=self.mean.get(),
                    std=self.std.get()
                )
            else:
                self.parameters.add_intervention(
                    type='masks',
                    day=self.day.get(),
                    mean=self.mean.get(),
                    std=self.std.get()
                )
                self.parameters.delete_intervention(type='masks', day=self.selected)
            self.destroy('Intervention successfully saved!')
        except Exception as e:
            self.destroy(e)   

class VentilationDialog(BasicDialog):

    def __init__(self, master, mode, parameters, selected=None):
        """ Create a new VentilationDialog window 
        
        Parameters:
        master      (tk.Widget)     : the element's master
        mode        (str)           : the dialog's operating mode
        parameters  (Parameters)    : the parameters associated with the element
        selected    (int)           : the day of the intervention selected from the dropdown menu

        """

        # Call the superclass' constructor
        super().__init__(master=master, parameters=parameters, title='Ventilation', mode=mode, serve='ventilation', selected=selected)

        Label(master=self, text='Enforce ventilation measures on the population', **text_style, wraplength=475).grid(row=0, column=0, columnspan=2, **spacing)

        # Create a day entry
        Label(master=self, text='Start Day', **text_style).grid(row=1, column=0, **spacing)
        self.day = NumberBox(master=self, low=0, high=1000, step=1, on_change=self.changed)
        self.day.grid(row=1, column=1, **spacing)

        Label(master=self, text='Protection offered by the ventilation measures', **text_style, wraplength=475).grid(row=2, column=0, columnspan=2, **spacing)

        # Create a mean entry
        Label(master=self, text='Protection (Mean)', **text_style).grid(row=3, column=0, **spacing)
        self.mean = NumberBox(master=self, low=0, high=1, step=0.001)
        self.mean.grid(row=3, column=1, **spacing)

        # Create a standard deviation entry
        Label(master=self, text='Protection (Standard Deviation)', **text_style).grid(row=4, column=0, **spacing)
        self.std = NumberBox(master=self, low=0, high=1, step=0.001)
        self.std.grid(row=4, column=1, **spacing)

        # Create the appropiate buttons
        if mode == 'add':
            Button(master=self, text='Cancel', command=self.cancel, **text_style).grid(row=5, column=0, **spacing)
            Button(master=self, text='Confirm', command=self.add, **text_style).grid(row=5, column=1, **spacing)
        elif mode == 'edit':
            Button(master=self, text='Cancel', command=self.cancel, **text_style).grid(row=5, column=0, columnspan=2, **spacing)
            Button(master=self, text='Delete', command=self.delete, **text_style).grid(row=6, column=0, **spacing)
            Button(master=self, text='Save', command=self.save, **text_style).grid(row=6, column=1, **spacing)
        else : raise ValueError('unrecognised mode')

        # Setup the window with initial values
        self.initialize()

        # Distribute any remaining space
        spread(self)

    def initialize(self):
        """ Initialize the dialog's values """

        if self.selected is not None:
            i = self.parameters.get_intervention('ventilation', self.selected)

            self.day.set(i['day'])
            self.mean.set(i['parameters']['mean'])
            self.std.set(i['parameters']['std'])

        self.changed()

    def changed(self):
        """ Validate entered values whenever one is changed """
        
        matches = [i for i in self.parameters.interventions if i['type'] == 'ventilation' and i['day'] == self.day.get() and i['day'] != self.selected]
        if len(matches) > 0 : self.day.configure(foreground='red')
        else : self.day.configure(foreground='black')
    
    def add(self):
        """ Add the new intervention """

        try:
            self.parameters.add_intervention(
                type='ventilation',
                day=self.day.get(),
                mean=self.mean.get(),
                std=self.std.get()
            )
            self.destroy('Intervention added!')
        except Exception as e:
            self.destroy(e)

    def delete(self):
        """ Delete the current intervention """

        try:
            self.parameters.delete_intervention(type='ventilation', day=self.selected)
            self.destroy('Intervention successfully deleted!')
        except Exception as e:
            self.destroy(e)

    def save(self):
        """ Save the current intervention """

        try:
            if self.day.get() == self.selected:
                self.parameters.delete_intervention(type='ventilation', day=self.selected)
                self.parameters.add_intervention(
                    type='ventilation',
                    day=self.day.get(),
                    mean=self.mean.get(),
                    std=self.std.get()
                )
            else:
                self.parameters.add_intervention(
                    type='ventilation',
                    day=self.day.get(),
                    mean=self.mean.get(),
                    std=self.std.get()
                )
                self.parameters.delete_intervention(type='ventilation', day=self.selected)
            self.destroy('Intervention successfully saved!')
        except Exception as e:
            self.destroy(e)   

class DistancingDialog(BasicDialog):

    def __init__(self, master, mode, parameters, selected=None):
        """ Create a new DistancingDialog window 
        
        Parameters:
        master      (tk.Widget)     : the element's master
        mode        (str)           : the dialog's operating mode
        parameters  (Parameters)    : the parameters associated with the element
        selected    (int)           : the day of the intervention selected from the dropdown menu

        """

        # Call the superclass' constructor
        super().__init__(master=master, parameters=parameters, title='Distancing', mode=mode, serve='distancing', selected=selected)

        Label(master=self, text='Enforce social distancing measures on the population', **text_style, wraplength=475).grid(row=0, column=0, columnspan=2, **spacing)

        # Create a day entry
        Label(master=self, text='Start Day', **text_style).grid(row=1, column=0, **spacing)
        self.day = NumberBox(master=self, low=0, high=1000, step=1, on_change=self.changed)
        self.day.grid(row=1, column=1, **spacing)

        Label(master=self, text='Protection offered by the social distancing measures', **text_style, wraplength=475).grid(row=2, column=0, columnspan=2, **spacing)

        # Create a mean entry
        Label(master=self, text='Protection (Mean)', **text_style).grid(row=3, column=0, **spacing)
        self.mean = NumberBox(master=self, low=0, high=1, step=0.001)
        self.mean.grid(row=3, column=1, **spacing)

        # Create a standard deviation entry
        Label(master=self, text='Protection (Standard Deviation)', **text_style).grid(row=4, column=0, **spacing)
        self.std = NumberBox(master=self, low=0, high=1, step=0.001)
        self.std.grid(row=4, column=1, **spacing)

        # Create the appropiate buttons
        if mode == 'add':
            Button(master=self, text='Cancel', command=self.cancel, **text_style).grid(row=5, column=0, **spacing)
            Button(master=self, text='Confirm', command=self.add, **text_style).grid(row=5, column=1, **spacing)
        elif mode == 'edit':
            Button(master=self, text='Cancel', command=self.cancel, **text_style).grid(row=5, column=0, columnspan=2, **spacing)
            Button(master=self, text='Delete', command=self.delete, **text_style).grid(row=6, column=0, **spacing)
            Button(master=self, text='Save', command=self.save, **text_style).grid(row=6, column=1, **spacing)
        else : raise ValueError('unrecognised mode')

        # Setup the window with initial values
        self.initialize()

        # Distribute any remaining space
        spread(self)

    def initialize(self):
        """ Initialize the dialog's values """

        if self.selected is not None:
            i = self.parameters.get_intervention('distancing', self.selected)

            self.day.set(i['day'])
            self.mean.set(i['parameters']['mean'])
            self.std.set(i['parameters']['std'])

        self.changed()

    def changed(self):
        """ Validate entered values whenever one is changed """
        
        matches = [i for i in self.parameters.interventions if i['type'] == 'distancing' and i['day'] == self.day.get() and i['day'] != self.selected]
        if len(matches) > 0 : self.day.configure(foreground='red')
        else : self.day.configure(foreground='black')
    
    def add(self):
        """ Add the new intervention """

        try:
            self.parameters.add_intervention(
                type='distancing',
                day=self.day.get(),
                mean=self.mean.get(),
                std=self.std.get()
            )
            self.destroy('Intervention added!')
        except Exception as e:
            self.destroy(e)

    def delete(self):
        """ Delete the current intervention """

        try:
            self.parameters.delete_intervention(type='distancing', day=self.selected)
            self.destroy('Intervention successfully deleted!')
        except Exception as e:
            self.destroy(e)

    def save(self):
        """ Save the current intervention """

        try:
            if self.day.get() == self.selected:
                self.parameters.delete_intervention(type='distancing', day=self.selected)
                self.parameters.add_intervention(
                    type='distancing',
                    day=self.day.get(),
                    mean=self.mean.get(),
                    std=self.std.get()
                )
            else:
                self.parameters.add_intervention(
                    type='distancing',
                    day=self.day.get(),
                    mean=self.mean.get(),
                    std=self.std.get()
                )
                self.parameters.delete_intervention(type='distancing', day=self.selected)
            self.destroy('Intervention successfully saved!')
        except Exception as e:
            self.destroy(e)   
