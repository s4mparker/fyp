from . import Simulation, Parameters, Results
from . import MenuBar, DiseasePanel, PopulationPanel, EnvironmentPanel, InterventionsPanel, OtherPanel, GraphPanel, LogPanel, ControlPanel, InstructionsPanel
from . import PanelButton
from . import spread
from tkinter import Tk

__all__ = ['Application']

class Application(Tk):

    width = 1500
    height = 820
    spacing = {'padx': 5, 'pady': 5, 'sticky': 'news'}

    def __init__(self):
        """ Create a new Application 
        
        Parameters:
        args    (list) : the list of arguments supplied when executing the program

        """

        # Initialize the superclass
        super().__init__()

        # Initialize the window
        self.title('Agent-Based Modelling of a Pandemic')
        self.geometry(f'{self.width}x{self.height}')
        self.resizable(False, False)
        
        # Create the objects to be used for the simulation
        self.simulation = Simulation()
        self.parameters = Parameters()
        self.simulation.set_parameters(self.parameters)

        # Create the user interface
        self.create_ui()

        # Enter the application's mainloop
        self.mainloop()

    def create_ui(self):

        # Create the panels
        self.instructions_panel = InstructionsPanel(self, force_x=20, force_y=600)
        self.disease_panel = DiseasePanel(self, force_x=20, force_y=600)
        self.population_panel = PopulationPanel(self, force_x=20, force_y=600)
        self.environment_panel = EnvironmentPanel(self, force_x=20, force_y=600)
        self.interventions_panel = InterventionsPanel(self, force_x=20, force_y=600)
        self.other_panel = OtherPanel(self, force_x=20, force_y=600)

        # Display the initial disease panel
        self.instructions_panel.grid(row=1, column=0, rowspan=2, columnspan=2, **self.spacing)

        # Create the toggle buttons
        PanelButton(master=self, text='Instructions', panel=self.instructions_panel, target=(1, 0), force_x=10, force_y=1).grid(row=0, column=0, **self.spacing)
        PanelButton(master=self, text='Disease', panel=self.disease_panel, target=(1, 0), force_x=10, force_y=1).grid(row=0, column=1, **self.spacing)
        PanelButton(master=self, text='Population', panel=self.population_panel, target=(1, 0), force_x=10, force_y=1).grid(row=0, column=2, **self.spacing)
        PanelButton(master=self, text='Environment', panel=self.environment_panel, target=(1, 0), force_x=10, force_y=1).grid(row=0, column=3, **self.spacing)
        PanelButton(master=self, text='Interventions', panel=self.interventions_panel, target=(1, 0), force_x=10, force_y=1).grid(row=0, column=4, **self.spacing)
        PanelButton(master=self, text='Other', panel=self.other_panel, target=(1, 0), force_x=10, force_y=1).grid(row=0, column=5, **self.spacing)

        # Create the graphing canvas
        self.graph_panel = GraphPanel(self, force_x=40, force_y=600)
        self.graph_panel.grid(row=1, column=2, rowspan=2, columnspan=4, **self.spacing)
        self.simulation.set_callback(self.graph_panel.refresh)

        # Create the log
        self.log_panel = LogPanel(self, force_x=20, force_y=150)
        self.log_panel.grid(row=3, column=0, rowspan=2, columnspan=2, **self.spacing)

        # Create the control panel
        self.control_panel = ControlPanel(master=self, force_x=40, force_y=150)
        self.control_panel.grid(row=3, column=2, rowspan=2, columnspan=4, **self.spacing)

        # Create the menu
        MenuBar(self)

        # Distribute any spare room in the window
        spread(self)

    def log(self, string):
        """ Add an entry to the application's log 
        
        Parameters:
        string  (str) : the string to be added to the log

        """

        self.log_panel.add(string)

    def run(self):
        """ Run a simulation using the provided parameters """

        try:
            self.simulation.run()
        except Exception as e:
            self.log(e)
            print(e.with_traceback())

    def reset(self):
        """ Reset a pre-existing simulation """

        try:
            self.simulation.reset()
        except Exception as e:
            self.log(e)
