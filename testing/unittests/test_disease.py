import unittest, copy
from simulation import Disease, InvalidSimulationParameters

class TestDisease(unittest.TestCase):

    standard = {
        'transitions': {
            'initial': {
                'exposed': 1,
            },
            'exposed': {
                'infectious': 1,
            },
            'infectious': {
                'symptomatic': 1,
            },
            'symptomatic': {
                'recovered': 1,
            }
        },
        'stages': {
            'exposed': {
                'length': {'mean': 5, 'std': 0},
                'infectivity': {'mean': 0.1, 'std': 0},
                'fatality': {'mean': 0, 'std': 0}
            },
            'infectious': {
                'length': {'mean': 5, 'std': 0.4},
                'infectivity': {'mean': 0.2, 'std': 0.1},
                'fatality': {'mean': 0.05, 'std': 0.02}
            },
            'symptomatic': {
                'length': {'mean': 7, 'std': 0.4},
                'infectivity': {'mean': 0.3, 'std': 0.1},
                'fatality': {'mean': 0.1, 'std': 0.02}
            },
            'recovered': {
                'length': {'mean': 14, 'std': 1},
                'infectivity': {'mean': 0, 'std': 0.001},
                'fatality': {'mean': 0, 'std': 0}
            }
        }
    }
 
    def test_okay_creation(self):
        d = Disease(self.standard['stages'], self.standard['transitions'])

    def test_bad_creation(self):

        for stage in self.standard['stages'].keys():

            p = copy.deepcopy(self.standard)
            p['stages'][stage] = {}
            self.assertRaises(InvalidSimulationParameters, Disease, stages=p['stages'], transitions=p['transitions'])
            
            p = copy.deepcopy(self.standard)
            p['stages'][stage]['length'] = {}
            self.assertRaises(InvalidSimulationParameters, Disease, stages=p['stages'], transitions=p['transitions'])

            p = copy.deepcopy(self.standard)
            p['stages'][stage]['length']['mean'] = -2
            self.assertRaises(InvalidSimulationParameters, Disease, stages=p['stages'], transitions=p['transitions'])

            p = copy.deepcopy(self.standard)
            p['stages'][stage]['length']['std'] = -0.5
            self.assertRaises(InvalidSimulationParameters, Disease, stages=p['stages'], transitions=p['transitions'])

            p = copy.deepcopy(self.standard)
            p['stages'][stage]['length']['mean'] = False
            self.assertRaises(InvalidSimulationParameters, Disease, stages=p['stages'], transitions=p['transitions'])

            p = copy.deepcopy(self.standard)
            p['stages'][stage]['length']['std'] = []
            self.assertRaises(InvalidSimulationParameters, Disease, stages=p['stages'], transitions=p['transitions'])

            p = copy.deepcopy(self.standard)
            p['stages'][stage]['infectivity'] = {}
            self.assertRaises(InvalidSimulationParameters, Disease, stages=p['stages'], transitions=p['transitions'])

            p = copy.deepcopy(self.standard)
            p['stages'][stage]['infectivity']['mean'] = -0.5
            self.assertRaises(InvalidSimulationParameters, Disease, stages=p['stages'], transitions=p['transitions'])

            p = copy.deepcopy(self.standard)
            p['stages'][stage]['infectivity']['std'] = -1
            self.assertRaises(InvalidSimulationParameters, Disease, stages=p['stages'], transitions=p['transitions'])

            p = copy.deepcopy(self.standard)
            p['stages'][stage]['infectivity']['mean'] = '-0.5'
            self.assertRaises(InvalidSimulationParameters, Disease, stages=p['stages'], transitions=p['transitions'])

            p = copy.deepcopy(self.standard)
            p['stages'][stage]['infectivity']['std'] = None
            self.assertRaises(InvalidSimulationParameters, Disease, stages=p['stages'], transitions=p['transitions'])

            p = copy.deepcopy(self.standard)
            p['stages'][stage]['fatality'] = {}
            self.assertRaises(InvalidSimulationParameters, Disease, stages=p['stages'], transitions=p['transitions'])

            p = copy.deepcopy(self.standard)
            p['stages'][stage]['fatality']['mean'] = -0.2
            self.assertRaises(InvalidSimulationParameters, Disease, stages=p['stages'], transitions=p['transitions'])

            p = copy.deepcopy(self.standard)
            p['stages'][stage]['fatality']['std'] = -0.01
            self.assertRaises(InvalidSimulationParameters, Disease, stages=p['stages'], transitions=p['transitions'])

            p = copy.deepcopy(self.standard)
            p['stages'][stage]['fatality']['mean'] = None
            self.assertRaises(InvalidSimulationParameters, Disease, stages=p['stages'], transitions=p['transitions'])

            p = copy.deepcopy(self.standard)
            p['stages'][stage]['fatality']['std'] = {}
            self.assertRaises(InvalidSimulationParameters, Disease, stages=p['stages'], transitions=p['transitions'])

    def test_progress(self):
        
        dis = Disease(stages=self.standard['stages'], transitions=self.standard['transitions'])

        p = dis.progress('initial')
        self.assertIsNotNone(p)
        (n, d, i, f) = p
        self.assertEqual(n, 'exposed')
        self.assertAlmostEqual(d, self.standard['stages'][n]['length']['mean'], delta=self.standard['stages'][n]['length']['std'] * 5)
        self.assertAlmostEqual(i, self.standard['stages'][n]['infectivity']['mean'], delta=self.standard['stages'][n]['infectivity']['std'] * 5)
        self.assertAlmostEqual(f, self.standard['stages'][n]['fatality']['mean'], delta=self.standard['stages'][n]['fatality']['std'] * 5)

        p = dis.progress('exposed')
        self.assertIsNotNone(p)
        (n, d, i, f) = p
        self.assertEqual(n, 'infectious')
        self.assertAlmostEqual(d, self.standard['stages'][n]['length']['mean'], delta=self.standard['stages'][n]['length']['std'] * 5)
        self.assertAlmostEqual(i, self.standard['stages'][n]['infectivity']['mean'], delta=self.standard['stages'][n]['infectivity']['std'] * 5)
        self.assertAlmostEqual(f, self.standard['stages'][n]['fatality']['mean'], delta=self.standard['stages'][n]['fatality']['std'] * 5)

        p = dis.progress('infectious')
        self.assertIsNotNone(p)
        (n, d, i, f) = p
        self.assertEqual(n, 'symptomatic')
        self.assertAlmostEqual(d, self.standard['stages'][n]['length']['mean'], delta=self.standard['stages'][n]['length']['std'] * 5)
        self.assertAlmostEqual(i, self.standard['stages'][n]['infectivity']['mean'], delta=self.standard['stages'][n]['infectivity']['std'] * 5)
        self.assertAlmostEqual(f, self.standard['stages'][n]['fatality']['mean'], delta=self.standard['stages'][n]['fatality']['std'] * 5)

        p = dis.progress('symptomatic')
        self.assertIsNotNone(p)
        (n, d, i, f) = p
        self.assertEqual(n, 'recovered')
        self.assertAlmostEqual(d, self.standard['stages'][n]['length']['mean'], delta=self.standard['stages'][n]['length']['std'] * 5)
        self.assertAlmostEqual(i, self.standard['stages'][n]['infectivity']['mean'], delta=self.standard['stages'][n]['infectivity']['std'] * 5)
        self.assertAlmostEqual(f, self.standard['stages'][n]['fatality']['mean'], delta=self.standard['stages'][n]['fatality']['std'] * 5)

        p = dis.progress('recovered')
        self.assertIsNone(p)

if __name__ == '__main__': 
    unittest.main()
