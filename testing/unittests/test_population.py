import unittest, copy
from simulation import *

class TestPopulation(unittest.TestCase):

    standard = {
        'student': {
            "quantity": 5,
            "mean": 0.5,
            "std": 0
        },
        'teacher': {
            "quantity": 3,
            "mean": 0.5,
            "std": 0.1
        }
    }

    def test_okay_creation(self):

        p = Population(agents=self.standard)

    def test_bad_creation(self):

        for agent in self.standard.keys():

            p = copy.deepcopy(self.standard)
            p[agent] = {}
            self.assertRaises(InvalidSimulationParameters, Population, agents=p)

            p = copy.deepcopy(self.standard)
            del p[agent]['quantity']
            self.assertRaises(InvalidSimulationParameters, Population, agents=p)

            p = copy.deepcopy(self.standard)
            del p[agent]['mean']
            self.assertRaises(InvalidSimulationParameters, Population, agents=p)

            p = copy.deepcopy(self.standard)
            del p[agent]['std']
            self.assertRaises(InvalidSimulationParameters, Population, agents=p)

            p = copy.deepcopy(self.standard)
            p[agent]['name'] = ''
            self.assertRaises(InvalidSimulationParameters, Population, agents=p)

            p = copy.deepcopy(self.standard)
            p[agent]['name'] = None
            self.assertRaises(InvalidSimulationParameters, Population, agents=p)

            p = copy.deepcopy(self.standard)
            p[agent]['quantity'] = ''
            self.assertRaises(InvalidSimulationParameters, Population, agents=p)

            p = copy.deepcopy(self.standard)
            p[agent]['quantity'] = -1
            self.assertRaises(InvalidSimulationParameters, Population, agents=p)

            p = copy.deepcopy(self.standard)
            p[agent]['mean'] = False
            self.assertRaises(InvalidSimulationParameters, Population, agents=p)

            p = copy.deepcopy(self.standard)
            p[agent]['mean'] = -2
            self.assertRaises(InvalidSimulationParameters, Population, agents=p)

            p = copy.deepcopy(self.standard)
            p[agent]['std'] = []
            self.assertRaises(InvalidSimulationParameters, Population, agents=p)

            p = copy.deepcopy(self.standard)
            p[agent]['std'] = -2
            self.assertRaises(InvalidSimulationParameters, Population, agents=p)

    def test_progress(self):

        p = Population(agents=self.standard)
        p = p + 1
        p = p + 2
        p = p + 100

    def test_infect(self):

        params = {
            'transitions': {
                'initial': {
                    'exposed': 1,
                },
                'exposed': {
                    'infectious': 1,
                },
                'infectious': {
                    'symptomatic': 1,
                },
                'symptomatic': {
                    'recovered': 1,
                }
            },
            'stages': {
                'exposed': {
                    'length': {'mean': 5, 'std': 0},
                    'infectivity': {'mean': 0.1, 'std': 0},
                    'fatality': {'mean': 0, 'std': 0}
                },
                'infectious': {
                    'length': {'mean': 5, 'std': 0.4},
                    'infectivity': {'mean': 0.2, 'std': 0.1},
                    'fatality': {'mean': 0.05, 'std': 0.02}
                },
                'symptomatic': {
                    'length': {'mean': 7, 'std': 0.4},
                    'infectivity': {'mean': 0.3, 'std': 0.1},
                    'fatality': {'mean': 0.1, 'std': 0.02}
                },
                'recovered': {
                    'length': {'mean': 14, 'std': 1},
                    'infectivity': {'mean': 0, 'std': 0.001},
                    'fatality': {'mean': 0, 'std': 0}
                }
            }
        }
        d = Disease(stages=params['stages'], transitions=params['transitions'])
        p = Population(agents=self.standard)
        p.randomly_infect(n=0, disease=d)
        p.randomly_infect(n=1, disease=d)
        p.randomly_infect(n=8, disease=d)
        self.assertRaises(InvalidSimulationRuntime, p.randomly_infect, n=-1, disease=d)
        self.assertRaises(InvalidSimulationRuntime, p.randomly_infect, n=9, disease=d)

    def test_test_agents(self):

        p = Population(agents=self.standard)
        p.test_agents(efficiency=1, coverage=1, isolation=10)
        p.test_agents(efficiency=0, coverage=0.5, isolation=1)
        p.test_agents(efficiency=0.25, coverage=0.75, isolation=100)
        self.assertRaises(InvalidSimulationRuntime, p.test_agents, efficiency=-1, coverage=0.75, isolation=100)
        self.assertRaises(InvalidSimulationRuntime, p.test_agents, efficiency=1, coverage=-15, isolation=100)
        self.assertRaises(InvalidSimulationRuntime, p.test_agents, efficiency=1, coverage=0.75, isolation=-200)

    def test_get_agents(self):

        p = Population(agents=self.standard)
        p.get_agents()

    def test_interact(self):

        p = Population(agents=self.standard)
        p.interact(0, 1)
        p.interact(1, 6)
        p.interact(7, 3)
        self.assertRaises(InvalidSimulationRuntime, p.interact, a=-1, b=2)
        self.assertRaises(InvalidSimulationRuntime, p.interact, a=1, b=100)
        self.assertRaises(InvalidSimulationRuntime, p.interact, a=4, b=4)

    def test_infect(self):

        p = Population(agents=self.standard)
        params = {
            'transitions': {
                'initial': {
                    'exposed': 1,
                },
                'exposed': {
                    'infectious': 1,
                },
                'infectious': {
                    'symptomatic': 1,
                },
                'symptomatic': {
                    'recovered': 1,
                }
            },
            'stages': {
                'exposed': {
                    'length': {'mean': 5, 'std': 0},
                    'infectivity': {'mean': 0.1, 'std': 0},
                    'fatality': {'mean': 0, 'std': 0}
                },
                'infectious': {
                    'length': {'mean': 5, 'std': 0.4},
                    'infectivity': {'mean': 0.2, 'std': 0.1},
                    'fatality': {'mean': 0.05, 'std': 0.02}
                },
                'symptomatic': {
                    'length': {'mean': 7, 'std': 0.4},
                    'infectivity': {'mean': 0.3, 'std': 0.1},
                    'fatality': {'mean': 0.1, 'std': 0.02}
                },
                'recovered': {
                    'length': {'mean': 14, 'std': 1},
                    'infectivity': {'mean': 0, 'std': 0.001},
                    'fatality': {'mean': 0, 'std': 0}
                }
            }
        }
        d = Disease(stages=params['stages'], transitions=params['transitions'])
        p.randomly_infect(n=8, disease=d)
        p.infection[0:2] = None
        self.assertRaises(InvalidSimulationRuntime, p.infect, infector=0, infected=1)
        self.assertRaises(InvalidSimulationRuntime, p.infect, infector=3, infected=4)
        p.infect(infector=2, infected=0)

if __name__ == '__main__': 
    unittest.main()
