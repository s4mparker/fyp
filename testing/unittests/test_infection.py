import unittest, copy
from simulation import *

class TestInfection(unittest.TestCase):

    standard = {
        'transitions': {
            'initial': {
                'exposed': 1,
            },
            'exposed': {
                'infectious': 1,
            },
            'infectious': {
                'symptomatic': 1,
            },
            'symptomatic': {
                'recovered': 1,
            }
        },
        'stages': {
            'exposed': {
                'length': {'mean': 5, 'std': 0},
                'infectivity': {'mean': 0.1, 'std': 0},
                'fatality': {'mean': 0, 'std': 0}
            },
            'infectious': {
                'length': {'mean': 5, 'std': 0.4},
                'infectivity': {'mean': 0.2, 'std': 0.01},
                'fatality': {'mean': 0.05, 'std': 0.02}
            },
            'symptomatic': {
                'length': {'mean': 7, 'std': 0.4},
                'infectivity': {'mean': 0.5, 'std': 0.1},
                'fatality': {'mean': 0.1, 'std': 0.02}
            },
            'recovered': {
                'length': {'mean': 14, 'std': 1},
                'infectivity': {'mean': 0, 'std': 0.001},
                'fatality': {'mean': 0, 'std': 0}
            }
        }
    }

    def test_okay_creation(self):

        d = Disease(stages=self.standard['stages'], transitions=self.standard['transitions'])
        i = Infection(d)

    def test_bad_creation(self):

        self.assertRaises(InvalidSimulationRuntime, Infection, None)

    def test_progress(self):

        d = Disease(stages=self.standard['stages'], transitions=self.standard['transitions'])
        i = Infection(d)
        self.assertEqual(i.remaining, 5)
        self.assertEqual(str(i), 'exposed')
        i = i + 1
        i = i + 2
        i = i + 4
        self.assertEqual(i.stage, 'infectious')
        self.assertEqual(str(i), 'infectious')
        self.assertEqual(i.elapsed, 7)
        i = i + 6
        self.assertEqual(i.stage, 'symptomatic')
        self.assertEqual(str(i), 'symptomatic')
        self.assertEqual(i.elapsed, 13)
        i = i + 100
        self.assertIsNone(i)

    def test_get_infectivity(self):

        d = Disease(stages=self.standard['stages'], transitions=self.standard['transitions'])
        i = Infection(d)
        self.assertEqual(i.get_infectivity(), 0.1)
        i = i + 7
        self.assertAlmostEqual(i.get_infectivity(), 0.2, delta=0.1)
        i = i + 7
        self.assertAlmostEqual(i.get_infectivity(), 0.5, delta=0.3)
        i = i + 10
        self.assertAlmostEqual(i.get_infectivity(), 0, delta=0.1)

    def test_survive(self):

        d = Disease(stages=self.standard['stages'], transitions=self.standard['transitions'])
        i = Infection(d)
        for v in range(100):
            self.assertEqual(i.survive(0), True)
            self.assertEqual(i.survive(1), True)
        
        p = copy.deepcopy(self.standard)
        p['stages']['exposed']['fatality']['mean'] = 1
        d = Disease(stages=p['stages'], transitions=p['transitions'])
        i = Infection(d)
        for v in range(100):
            self.assertEqual(i.survive(0), False)
            self.assertEqual(i.survive(1), False)
        
    def test_get_disease(self):

        d = Disease(stages=self.standard['stages'], transitions=self.standard['transitions'])
        i = Infection(d)
        self.assertIsInstance(i.get_disease(), Disease)