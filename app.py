""" External """
import sys

""" Internal """
from application import Application

""" 

This file will load the application's user interface

"""

app = Application()