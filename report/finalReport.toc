\contentsline {chapter}{\numberline {1}Introduction}{1}%
\contentsline {chapter}{\numberline {2}Literature Review}{2}%
\contentsline {section}{\numberline {2.1}Definitions}{2}%
\contentsline {section}{\numberline {2.2}A brief history of pandemic modelling \& simulations}{2}%
\contentsline {section}{\numberline {2.3}Using simulations to understand COVID-19}{3}%
\contentsline {section}{\numberline {2.4}Agent-based models \& simulations}{4}%
\contentsline {subsection}{\numberline {2.4.1}Advantages of agent-based approaches}{4}%
\contentsline {paragraph}{Emergent Behaviour}{5}%
\contentsline {paragraph}{Natural Description}{5}%
\contentsline {paragraph}{Flexibility}{5}%
\contentsline {subsection}{\numberline {2.4.2}Disadvantages of agent-based approaches}{5}%
\contentsline {subsection}{\numberline {2.4.3}Agent-based modelling of a pandemic}{6}%
\contentsline {section}{\numberline {2.5}Simulating stochastic processes}{6}%
\contentsline {section}{\numberline {2.6}Motivations}{6}%
\contentsline {subsection}{\numberline {2.6.1}Why is such a flexible simulation tool required?}{6}%
\contentsline {subsection}{\numberline {2.6.2}Why is agent-based modelling the best approach for such a tool?}{7}%
\contentsline {chapter}{\numberline {3}Project Outline}{8}%
\contentsline {section}{\numberline {3.1}Project Specification}{8}%
\contentsline {section}{\numberline {3.2}Project Objectives}{8}%
\contentsline {section}{\numberline {3.3}Development Approach \& Methodology}{8}%
\contentsline {section}{\numberline {3.4}Project Timeline}{9}%
\contentsline {section}{\numberline {3.5}Development Tools}{9}%
\contentsline {subsection}{\numberline {3.5.1}Language Selection}{9}%
\contentsline {chapter}{\numberline {4}Design}{11}%
\contentsline {section}{\numberline {4.1}Solution Architecture}{11}%
\contentsline {section}{\numberline {4.2}Requirements Gathering}{11}%
\contentsline {subsection}{\numberline {4.2.1}Stakeholder Discussion}{11}%
\contentsline {subsection}{\numberline {4.2.2}Analysis of Existing Solutions}{11}%
\contentsline {subsection}{\numberline {4.2.3}Use Cases \& User Stories}{11}%
\contentsline {section}{\numberline {4.3}Requirements}{12}%
\contentsline {subsection}{\numberline {4.3.1}Rich Picture}{12}%
\contentsline {section}{\numberline {4.4}Model Outline}{13}%
\contentsline {section}{\numberline {4.5}Interface Mock-ups}{14}%
\contentsline {chapter}{\numberline {5}Implementation}{16}%
\contentsline {section}{\numberline {5.1}Model}{16}%
\contentsline {subsection}{\numberline {5.1.1}Summary}{16}%
\contentsline {subsection}{\numberline {5.1.2}UML Diagram}{16}%
\contentsline {subsection}{\numberline {5.1.3}Simulation}{16}%
\contentsline {subsection}{\numberline {5.1.4}Model}{17}%
\contentsline {subsection}{\numberline {5.1.5}Parameters}{17}%
\contentsline {subsection}{\numberline {5.1.6}Results}{17}%
\contentsline {subsection}{\numberline {5.1.7}Population}{18}%
\contentsline {subsection}{\numberline {5.1.8}Disease \& Infection}{19}%
\contentsline {subsection}{\numberline {5.1.9}Environment}{19}%
\contentsline {subsection}{\numberline {5.1.10}Interventions}{19}%
\contentsline {subsection}{\numberline {5.1.11}Performing Monte Carlo Experiments}{20}%
\contentsline {section}{\numberline {5.2}User Interface \& Application}{21}%
\contentsline {subsection}{\numberline {5.2.1}Summary}{21}%
\contentsline {subsection}{\numberline {5.2.2}UML Diagram}{21}%
\contentsline {subsection}{\numberline {5.2.3}Wrapper Widgets}{21}%
\contentsline {subsection}{\numberline {5.2.4}Interface Components}{21}%
\contentsline {subsection}{\numberline {5.2.5}Constructing Visualizations}{22}%
\contentsline {chapter}{\numberline {6}Evaluation Methods}{23}%
\contentsline {section}{\numberline {6.1}Case Study: UK School}{23}%
\contentsline {subsection}{\numberline {6.1.1}Gathering COVID-19 data}{24}%
\contentsline {subsection}{\numberline {6.1.2}Designing \& implementing the environment}{24}%
\contentsline {section}{\numberline {6.2}Benchmarking}{25}%
\contentsline {section}{\numberline {6.3}User Evaluation}{25}%
\contentsline {chapter}{\numberline {7}Results \& Discussion}{26}%
\contentsline {section}{\numberline {7.1}Final Solution}{26}%
\contentsline {section}{\numberline {7.2}Testing}{26}%
\contentsline {section}{\numberline {7.3}Case Study Results}{26}%
\contentsline {section}{\numberline {7.4}Benchmarking Results}{27}%
\contentsline {section}{\numberline {7.5}User Evaluation Results}{28}%
\contentsline {section}{\numberline {7.6}Conclusion}{29}%
\contentsline {section}{\numberline {7.7}Ideas for Future Work}{29}%
\contentsline {chapter}{References}{30}%
\contentsline {chapter}{Appendices}{34}%
\contentsline {chapter}{\numberline {A}Self-appraisal}{34}%
\contentsline {section}{\numberline {A.1}Critical self-evaluation}{34}%
\contentsline {subsection}{\numberline {A.1.1}Strengths}{34}%
\contentsline {section}{\numberline {A.2}Personal reflection and lessons learned}{35}%
\contentsline {subsection}{\numberline {A.2.1}Lessons Learnt}{36}%
\contentsline {section}{\numberline {A.3}Legal, social, ethical and professional issues}{36}%
\contentsline {subsection}{\numberline {A.3.1}Legal issues}{36}%
\contentsline {subsection}{\numberline {A.3.2}Social issues}{37}%
\contentsline {subsection}{\numberline {A.3.3}Ethical issues}{37}%
\contentsline {subsection}{\numberline {A.3.4}Professional issues}{38}%
\contentsline {chapter}{\numberline {B}External Material}{39}%
\contentsline {chapter}{\numberline {C}Development Tools}{40}%
\contentsline {chapter}{\numberline {D}Use Case \& User Stories}{43}%
\contentsline {chapter}{\numberline {E}Model Parameters}{44}%
\contentsline {chapter}{\numberline {F}UI Mockups}{46}%
\contentsline {chapter}{\numberline {G}Visualization Mockups}{50}%
\contentsline {chapter}{\numberline {H}Deployment Diagram}{52}%
\contentsline {chapter}{\numberline {I}Case Study}{53}%
\contentsline {section}{\numberline {I.1}COVID-19 Parameters}{53}%
\contentsline {section}{\numberline {I.2}School Environment}{53}%
\contentsline {section}{\numberline {I.3}Results}{56}%
\contentsline {chapter}{\numberline {J}Questionnaire}{58}%
\contentsline {chapter}{\numberline {K}Unit Tests}{62}%
\contentsline {chapter}{\numberline {L}Benchmarking Results}{64}%
\contentsline {chapter}{\numberline {M}Final Solution}{66}%
