\justifying
% maybe add a note on testing
%\paragraph{Testing}
%Unit testing will be employed throughout the development process to ensure individual components function as expected in isolation; further end-to-end testing will occur post-development using the use cases defined in Chapter \ref{use_cases}. Python's unittest package will be used to define and execute unit tests (see Chapter \ref{tools}).

\chapter{Project Outline}
This chapter will outline the project's purpose and explore the organization of the work to ensure good software development principles are followed.

\section{Project Specification}
\label{project_summary}
The project aims to develop an interactive agent-based simulation tool through which a user can explore the impact different interventions have on the spread of disease. The user will be able to define their own microcosms in which different types of agents can exist - this flexibility will allow the tool to be used to discover the optimal strategies in which transmission can be reduced in different scenarios or settings. 

As discussed in Chapter \ref{monte_carlo} the process of disease transmission is an inherently random process - meaning that there will exist variability in the results produced by a tool attempting to model such a system. To counteract this unpredictability and develop a solution which produces accurate and reproducible results, Monte Carlo simulations will be used to calculate the approximate expected outcome of a scenario; this technique will enable more confidence to be placed in results produced by the tool.

\section{Project Objectives}
\label{project_objectives}
The sole objective of the project is to develop an application that can be used to perform pandemic simulations in user-defined environments.

\section{Development Approach \& Methodology}
\label{development_approach}
% maybe add why agile was not selected?

The development of the solution was primarily split into two main components: the underlying pandemic model and the application's user interface.
\begin{description}
	\item[Model Development] The first phase of development will focus on developing a suitable and accurate pandemic model which can be used to perform simulations on user-defined environment.
	\item[User Interface] The second phase of development will focus on creating an interactive user interface through which the user can interact with the pandemic model developed in phase one.
\end{description}
The modular approach to development was selected to ensure that the solution's modelling logic and presentation were functionally independent; this is a well known software engineering practice\footnote{Commonly referred to as the separation of concerns.} used to develop modular and flexible applications that are constructed from loosely coupled yet highly cohesive components.

Based on the plan to split development into two distinct phases, a waterfall methodology was selected to be the most suitable for the project. This decision was motivated based on the fact that the solution's requirements (see Chapter \ref{requirements}) were clearly defined from an early stage and unlikely to materially change throughout development. In addition a common feature of waterfall-centric projects is the well defined transfer of information between stages; the two stages of the project would benefit from this clear transfer when the model developed in the phase one was used in phase two to construct the wider simulation application.

However, the use of a primarily waterfall-based approach did not rule out the possibility of using some agile principles throughout the project. It was expected that once both phases of the project reached their conclusion and the application was nearly complete, a short iterative process would take place to perform small and rapid improvements to the product. This was not expected to be a formal agile process - rather an opportunity to apply finishing touches to the solution prior to evaluation.

\section{Project Timeline}
\label{project_timeline}
A Gantt chart is included in Figure \ref{gantt_chart}; this was created early in the project to provide an overview of the work that was expected to take place.

\begin{figure}[h]
	\centering
	\includegraphics[width=1.0\textwidth]{./figures/gantt.png}
	\caption{Gantt Chart}
	\label{gantt_chart}
\end{figure}

\section{Development Tools}
\label{development_tools}
During the project, a number of supplementary tools were used to assign development and promote good software engineering practices. A short summary of these tools is included in Table \ref{development_tools}; evidence demonstrating the use of the described tools is included in Appendix \ref{tools}.

\begin{table}[h]
	\centering
	\begin{tabular}{ p{0.15\textwidth} | p {0.15\textwidth} | p{0.7\textwidth} }
		
		\rule{0pt}{3.2ex} \textbf{Tool Name} & \textbf{Purpose} & \textbf{Description} \\ [1ex]
		\hline
		
		\rule{0pt}{3.2ex} Gitlab & Version Control \& Remote Backup & Gitlab will be used for version control and to maintain remote back-ups of the project's progress. Regular commits will be made to the project repository to save regular 'snapshots' of the work done and in order to offer a contingency plan in the unfortunate event of data loss or corruption.  \\ [1ex]
		
		\rule{0pt}{3.2ex} VSCode & Development \& Debugging & VSCode will be used throughout the development process to provide access to a range of helpful software development tools; including but not limited to a debugger, auto-completion and external third party extensions.  \\ [1ex]
		
		\rule{0pt}{3.2ex} Trello & Project Management & Trello will be used to maintain a Kanban board which will support good project management practices and offer an interactive view of progress. Cards will transition through the different stages of the board as work is completed and new cards will be added when new functionality and requirements are discovered. \\ [1ex]
		
		\rule{0pt}{3.2ex} unittest & Unit Testing & Python's unittest package will be used during development to write and manage component unit tests. After each piece of functionality is completed, a collection of tests will be written to ensure the component functions as expected in isolation. As development progresses and an increasing number of unit tests are developed, regression testing will become possible to ensure that new changes to the codebase don't accidentally break existing functionality. \\ [1ex]
		
	\end{tabular}
	\caption{Development Tools}
	\label{tools}
\end{table}

\subsection{Language Selection}
When selecting a language for development, it was critical that an object-oriented paradigm was supported; this would enable us to approach the project in a modular way and develop the solution component-by-component. Based on this requirement there emerged three possible contenders: Java, C++ or Python. Out of these options, Python was selected to be the primary development language. This was motivated by three factors.

\begin{itemize}
	\item Python offered the most readable and easily-maintainable syntax. This would be important during the later stages of the project when the solution was bound to become complex and modular.
	\item The author's previous experience with the language.
	\item Python supported a wide range of third-party packages that would assist with delivering the final solution. Alternatives to these were available with the other options, however they were significantly more complex.
\end{itemize}

However general programming languages were not the only option that could be used to develop an pandemic simulation tool. A number of specialist tools can be used to construct complex agent-based models~\cite{abm_options}; a commonly used solution is Netlogo~\cite{netlogo} which offers users a programmable modelling environment that can be applied to agent-based systems. The open-source tool provides its own programming language and IDE for developers to use and supports spatial modelling, parallel executions and a range of other features commonly used in agent-based models. However despite the benefits associated with a language such as Netlogo, a general programming language offers flexibility that simply can't be matched by such a specialist tool. In addition to this, the author already had extensive experience with using Python - in comparison using a tool such as Netlogo would have required learning an entirely new language which could have proved difficult to master and apply in a project with this time frame.

