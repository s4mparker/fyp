\chapter{Background Research}\label{background_research}

This chapter will investigate the use of pandemic modelling as a tool for understanding the complex transmission dynamics behind a pandemic and will explore why such a practice is often used to inform strategic policy decisions. The text will discuss three common approaches to modelling the spread of disease: deterministic, stochastic and agent-based, with a short look at the SIR model which is the one of most well known applications of deterministic models to modern pandemics. The latter sections of the chapter will explore the need for a tool which can be used to understand transmission dynamics within a complex microcosm and will attempt to justify why an agent-based approach is best suited for this application.

\section{Applications of Pandemic Modelling}

\paragraph{}

In January 2020, a novel coronavirus was identified in Wuhan, China by Chinese authorities following a series of pneumonia cases with unknown origin~\cite{first_case}. Belonging to the same family of disease as the common cold, this newly identified strain caused those exposed to exhibit a variety of symptoms, ranging from a mild cough to severe breathing difficulties~\cite{symptoms}. The virus, shortly afterwards named SARS-CoV-2\footnote{Commonly referred to as COVID-19}, rapidly spread internationally and on March 11th the situation was declared a pandemic by the World Health Organization~\cite{pandemic_announcement}.

\paragraph{}

As the virus continued spreading exponentially and the stark reality of the situation came to light, focus quickly turned to controlling and eventually stopping the virus. Research began at an unprecedented scale in an effort to understand the disease's transmission and development of a vaccine began in an attempt to provide a long-term solution to the situation. However by the start of March a readily available vaccine was still months away~\cite{vaccine_delay}, and with nearly 100,000 cases and 3,000 deaths to date~\cite{cases_and_deaths}, a short term solution was required to slow the highly concerning growth of the virus in the immediate future.

\paragraph{}

One of the most effective short term solutions to the spread of a virus is the implementation of non-pharmaceutical interventions\footnote{Actions and measures, besides getting vaccinated or taking medicine, which reduce the spread of an illness}. These policies enable the transmissibility of a virus to be drastically reduced without medical support - resulting in less infections and deaths; however this is often achieved with sacrifice or inconvenience elsewhere to those subjected to the measures. In the case of COVID-19 and due to the fatal nature of the virus, governments worldwide responded with a range of NPIs in a valiant effort to curb the virus' growth. On the 23rd March 2020, the UK Government announced the imposal of a series of unprecedented NPIs on the British population~\cite{uk_response}, and by the start of April nearly all countries in the world had implemented at least some public health measures on their respective populations~\cite{world_response}.

\paragraph{}

However, the implementation of non-pharmaceutical interventions is not always a straightforward decision and often results in a challenging balancing act for policy makers. A full-scale lockdown, one of the most drastic NPIs available and one observed during COVID-19, has been shown to cause intense social isolation~\cite{social_isolation} and a rise in physical inactivity for those affected~\cite{physical_activity}; previous research has also shown that such a move can also have lasting economical consequences too~\cite{economic_impact}. Even less 'intense' measures can have adverse effects too - prolonged mask wearing has been shown to cause headaches, impaired cognition and skin breakdown~\cite{mask_wearing} while social distancing has been demonstrated to negatively impact a person's well-being~\cite{social_distancing}. The decision to impose new non-pharmaceutical interventions can't be taken lightly, and there must be sufficient evidence in favour of the measure and its impact on reducing infections to warrant the negative consequences that accompany it. In order to explore whether a certain measure is justified, policy makers turn to a variety of sources to inform their decision making; pandemic modelling is increasingly becoming one of these.

\paragraph{}

\label{modelling}

Pandemic modelling is a relatively new practice that is used to understand the course of a pandemic and plan effective control strategies, including the exploration of various NPIs. A representation of the interactions and behaviours influencing the spread of a virus is constructed, which is then used to conduct 'what-if' analysis\footnote{A powerful form of analysis which involves investigating the impact of changing one or more variables in a system} using potential scenarios and forecast future cases. Such a technique enables a unique understanding of the dynamics behind a pandemic to be developed, and is frequently used to explore the effect different measures have on the spread of a disease. This bigger picture of such a complex phenomena is becoming increasingly invaluable in modern pandemic response planning and is beginning to play a significant role in determining the most effective measures to control the spread of disease.

\section{A brief history of Pandemic Modelling}

\paragraph{}

The desire to understand and track the spread of disease has been present throughout history, with one of the earliest examples dating back to the late 16{\scriptsize th} century. The Bills of Mortality were a collection of documents published weekly from 1592, initially used to track burials but later expanded to include the cause of death, across the 130 London parishes. The documents represented one of the earliest attempts to monitor public health and acted as an early warning system for any potential disease outbreaks~\cite{bills_of_mortality}. Daniel Bernoulli is credited with developing the first epidemiological model in the 18{\scriptsize th} century, which he used to study the impact of smallpox variolation\footnote{An early form of vaccination} and the impact it could have on life expectancy if the disease was to be eradicated. Bernoulli's work found that inoculation was beneficial if the risk of death was <11\%, and although it was criticized by French mathematician Jean le Rond d'Alembert who developed his own alternative method, it proved an important first step in epidemiological modelling~\cite{daniel_bernoulli}.

\paragraph{}

\label{sir}

In the early 20{\scriptsize th} century ideas surrounding epidemiological modelling began to be formalized. In 1906, W.H. Hamer proposed the idea that the spread of an infection could be influenced by two factors: the number of infected individuals and the number of susceptible individuals~\cite{hamer}. This suggestion formed the basis of the compartmental model, which separated the population into distinct 'labelled' compartments and involved members transitioning between the states to represent the dynamics of the disease. One particularly interesting and early case of compartmental models being used was by R.A. Ross who used the same ideas of distinct 'compartments' to understand the transmission dynamics of malaria; despite the widely held belief being at the time that malaria could not be eradicated without the removal of mosquitoes from the environment, Ross disproved this idea by showing that a sufficient reduction would achieve the same goal~\cite{ross}. Finally in the 1927, A.G. McKendrick and W.O. Kermack published the first of three papers formalizing the idea of a compartmental epidemic model which could be used to describe the transmission of communicable diseases~\cite{kermack_mckendrick_1, kermack_mckendrick_2, kermack_mckendrick_3} - their work has since formed the cornerstone of modern pandemic modelling and numerous extensions have been made to the original Kermack-McKendrick model since its first formalization.

\paragraph{}

With the idea of compartmental models beginning to take shape and the Kermack-McKendrick model being published, there were some serious shortcoming identified in the approach which prompted a new type of stochastic model to be produced. Critics of the Kermack-McKendrick model pointed out the flawed assumption of homogeneous mixing in the model, which while adequate when presented with a large number of infectious individuals, failed to sufficiently represent the spread of a disease in its early stages when transmission is a random event~\cite{pitfalls}. This prompted the idea of a new type of process to be developed that instead modelled the population as a network of contacts, with infectious agents infecting their neighbours and so on. The work, later known as a branching process, was built on top of studies done by Watson and Galton in the 19{\scriptsize th} century to explore the extinction of family names~\cite{galton} and which used similar dynamics to those observed during a disease outbreak. In a series of lectures in 1928, L. Reed and W.H. Frost further expanded on this idea of branching processes, discussing a chain binomial method to be used for disease transmission; their work however was not formalized until the 1950s~\cite{reed_frost}. The Reed-Frost model, similar to the Kermack-McKendrick model, formed an important foundation for modern techniques used to understand the spread of disease.

\begin{figure}[htpb]
	\centering
	\includegraphics[width=\textwidth, height=3cm, keepaspectratio=true]{figures/branching_process.png}
	\caption{A demonstration of the simple dynamics behind a branching process, from ~\cite{branching_process}}
\end{figure}

Since the introduction of both deterministic and stochastic models in the early 20{\scriptsize th} century, extensive research into extensions for both model types has taken place. The deterministic Kermack-McKendrick model was an early form of the SIR model, which is among the most widely used pandemic modelling techniques used today. The SIR model itself has also since seen further variations, including the SEIR and SIRV which add exposed and vaccinated compartments respectively. 

\paragraph{}

In the latter half of the century however, an unparalleled rise in computational power enabled a third type of pandemic model, agent-based, to be introduced. Explored more in section \ref{agent-based} of this paper, early examples of agent-based models include Thomas Schelling's segregation model\footnote{Originally Schelling used coins and paper rather than computers, however his work is one of the earliest demonstrations of agent-based concepts}~\cite{shelling} and Craig Reynold's work on modelling 'flocking' behaviour in birds~\cite{reynolds}; the first notable case of an agent-based model capable for being applied to the transmission of disease was Sugarscape in the 1990s~\cite{sugarscape}. Since then the use of agent-based models in relation to pandemic modelling has only continued to grow - culminating in a surge in interest during the coronavirus pandemic in 2020 which saw numerous agent-based models being produced in an attempt to understand the virus.

\section{A deeper look into Agent-Based Modelling}
\label{agent-based}

\paragraph{Summary}

Agent-based modelling (ABM) is a relatively new technique that has emerged as a result of the increase in computational processing power that has been observed over the past few decades. The approach attempts to model a wider system as a collection of its constituent components; each of these components is represented by an individual, autonomous agent in the representation. Each agent is governed by a set of rules which determine the interactions the agent can have with other agents and its environment - these rules are predefined by the model creator based on the environment being modelled. ABMs can be used to model complex dynamics present within a system that may be out of reach of traditional mathematical models, or potentially may even expose hidden behaviours which were previously unrecognised.

\subsubsection{Emergent Behaviour}

Due to the use of individual and autonomous entities in a model and its bottom-up design, behaviour present in an agent-based system is an collective result of its components' behaviour. This therefore allows complex emergent phenomena that is only produced by the collective interactions between agents to be identified; this type of behaviour is often unpredictable, meaning other modelling methods may mis-model it or potentially even ignore it. Using agent-based techniques to identify these cases can help us to recognize new patterns of behaviour among agents and offer us a new perspective on systems we previously thought we understood.

\subsubsection{Natural Description of a System}

Modelling an environment out of its constituent parts offers us a natural and intuitive way to think about system design. With traditional modelling techniques, the system has to be modelled as a collective sum of behaviours which can often be challenging and tough to achieve accurately; in comparison, using an agent-based design allows for the model to be designed from the perspective of the individual components and then the unique behaviours assembled naturally by the model as interactions occur. This change in perspective simplifies the design process significantly.

\subsubsection{Flexibility}

Due to their composition, by design an agent-based system is flexible. Additional agents easily be added to a model

\section{Monte Carlo Simulations}\label{monte_carlo}

When modelling an inherently random process such as a pandemic, there will inevitability exist variability in the final results due to the random variables that are used in the process. This can often lead to multiple runs of a model, all of which use identical parameters, to output varying results. This unpredictability has obvious consequences to a model and its credibility; a model which outputs a different result each time offers no valid or robust results which can be used to support decision-making.

\paragraph{}

In order to solve this problem associated with modelling random processes, a collection of methods called Monte Carlo simulations can be used. In broad terms, the techniques attempt to find the approximate solution to a problem by repeatedly randomly sampling variables - the expected (and approximated) outcome can then be calculated as an average of the produced results. In terms of modelling a pandemic, this often involves performing multiple executions of a model in order to calculate the expected number of cases arising from a defined scenario. This technique, while at its heart simple, reduces the role that underlying variability has on a model's output and allows for more confidence in the predictions made using stochastic models.

\section{A summary of infectious diseases}

\section{Motivations}
\subsection{Why do we need to model microcosms?}

Since its discovery in January 2020, COVID-19 became a near-constant factor in nearly everybody's day-to-day lives. In August 2021, an effective vaccination was approved by the FDA for widespread use and by April 2022 approximately 59\% of the world's population had received at least two doses of an effective vaccine. Unfortunately this didn't mean the virus had disappeared - as time passed new strains were identified, with the most notable being Omicron in December 2021 which was x\% more infectious than the original strain. This meant cases numbers were still present, and even in early 2022 there was still around one million new cases a day. Despite this, similar measures alike to those previously seen were not implemented 

Over the past two years since its discovery, COVID-19 has become a constant presence in nearly everybody's day-to-day lives and has wrecked untold havok around the world. With a vaccine readily available and life beginning to return to some form of normality, we can no longer allow the virus to disrupt our lives in the way it has previously and instead must learn to live with it. However this does not mean the virus has disappeared - there 


\subsection{Why do we use agent-based models?}

Agent-based models were selected for this project over the alternative options for a variety. 



