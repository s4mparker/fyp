\justifying

\chapter{Design}
\label{design}
\section{Solution Architecture}
As discussed in Chapter \ref{development_approach}, the solution is split into two components.

\begin{itemize}
	\item An agent-based pandemic model; this will be used to simulate disease transmission,
	\item An interactive user interface through which the user interacts with the underlying pandemic model
\end{itemize}

The application will deployed solely as a client application; this is included in the deployment diagram included in Appendix \ref{deployment_diagram}.

\section{Requirements Gathering}\label{requirements_gathering}
To understand the solution's requirements, a series of requirements gathering processes were undertaken to identify and explore the functionality expected from the solution.

\subsection{Stakeholder Discussion}
The ideal method of identify the solution's requirements would have been to directly interact with potential users of the product. Unfortunately this wasn't possible in the context of the project; this meant alternative options had to be pursued in order to understand the solution's requirements.

\subsection{Analysis of Existing Solutions}
A good alternative to directly meeting end users of the product was to investigate similar existing solutions. As discussed in Chapter \ref{covasim}, Covasim is an agent-based modelling tool that was used during the COVID-19 crisis to plan effective measures in a number of countries. 

\subsection{Use Cases \& User Stories}
To support the gathering of requirements, a use case diagram was developed along with a series of user stories. These enabled the solution to be approached from the perspective of the user and understand the functionality that stakeholders expected from the product. Furthermore, they assisted in the formalization of some ideas that were raised during the analysis of existing solutions and collectively reiterated the importance of interactivity and ease-of-use in the final solution. Both of these items were also helpful later in the project during the evaluation of the solution to identify whether the project's objective had been achieved.

The developed use case diagram is included in Figure \ref{use_case_diagram} and the developed user stories are included in Figure \ref{user_stories}. 

\section{Requirements}
Following the requirements gathering process described in Chapter \ref{requirements_gathering}, the solution's requirements could begin to be defined. Each requirement was assigned an acceptance criteria; this provided a measurable way that could be used during the solution's evaluation to determine if each requirement had been achieved.

\begin{table}[h]
	\centering
	\begin{tabular}{p{0.15\textwidth} | p{0.05\textwidth} | p{0.4\textwidth} }
		
		\rule{0pt}{3.2ex} \textbf{Type} & \textbf{ID} & \textbf{Requirement} \\ [1ex]
		\hline
		\rule{0pt}{3.2ex} Functional & 1 & The user must be able perform pandemic simulations which forecast the predicted case numbers and fatalities  \\ [1ex]
		\rule{0pt}{3.2ex} Functional & 2 & The user must be able to define their own environments which can be used with the model \\ [1ex]
		\rule{0pt}{3.2ex} Functional & 3 & The user must be able to use the solution to model a variety of diseases \\ [1ex]
		\rule{0pt}{3.2ex} Functional & 4 & The user must be able to impose a range of interventions on a pandemic simulation  \\ [1ex]
		\rule{0pt}{3.2ex} Functional & 5 & The product should output at least two visualizations summarising the results of a simulation  \\ [1ex]
		\rule{0pt}{3.2ex} Non-Functional & 6 & The user must be able to perform a simulation in a reasonable length of time  \\ [1ex]
		\rule{0pt}{3.2ex} Non-Functional & 7 & The product should provide an interface which enables interaction with the underlying model \\ [1ex]
		
	\end{tabular}
	\caption{Solution requirements and acceptance criteria}
\end{table}

\subsection{Rich Picture}
To identify the key requirements of the produced model, a rich picture was developed to deepen our understanding of the entities and behaviours in the system being modelled. This procedure, while often overlooked during model development, offers an opportunity to explore the relationships present in the underlying system and to formalize ideas surrounding key system dynamics. The produced rich picture is included in Appendix \ref{rich_picture}. \linebreak

In the context of disease transmission, constructing a rich picture enabled the quick identification of agents\footnote{Agent refers to a human agent in this context, not the autonomous agent described by agent-based techniques.} and infections as the primary entities in the system. Furthermore through this analysis the relationships between the two types of agent and their environment became apparent; these ideas could then be used to begin to formalize the agents and behaviours that our model would be composed of.

\section{Model Outline}
\label{model_outline}
The model will simulate the contact-based transmission of a given disease over a finite number of discrete days. The model will contain a population of individual agents and will require the user to define an environment to represent the underlying microcosm that the simulation occurs in. 

An environment defines two key components: the type \& number of agents present in population and the rules of interaction that will be used to calculate the inter-agent interactions that will be simulated when the model is executed. By allowing these two components to be fully customized to the user's needs, the model provides a flexible approach to modelling that can be used to represent varying microcosms with the same underlying model. This supports the fulfilment of requirements.

As discussed, the model's population will contain a series of individual agents constructed using agent-based principles. Each agent will have a state; this will be determined by a series of attributes described in Table \ref{agent_attributes}. Each agent will be assigned a type; the possible agent types will be defined by the environment the user opts to use for a simulation. Each agent has the same attributes - the agent type simply determines an individual agent's vulnerability which itself influences an agent's susceptibility to infection.

\begin{table}[h]
	\centering
	\begin{tabular}{p{0.2\textwidth} | p{0.7\textwidth}}
		
		\rule{0pt}{3.2ex} \textbf{Attribute} & \textbf{Description} \\ [1ex]
		\hline
		\rule{0pt}{3.2ex} $ active $ &  Agent's active state\\ [1ex]
		\rule{0pt}{3.2ex} $ type $ & Agent's type \\ [1ex]
		\rule{0pt}{3.2ex} $ vulnerability $ & Agent's vulnerability to infection and passing \\ [1ex]
		\rule{0pt}{3.2ex} $ space $ & Agent's current space \\ [1ex]
		\rule{0pt}{3.2ex} $ infection $ & Agent's infection status \\ [1ex]
		
	\end{tabular}
	\caption{Agent attributes}
	\label{agent_attributes}
\end{table}

The $ infection $ attribute of an agent will be used to store the agent's infection status. When an agent interacts with an infected agent, there will be a stochastic calculation performed to determine whether the transmission was successful. If so, the $ infection $ attribute of an agent will be set to reflect the agent's newly infected status. \linebreak

A disease will be composed of a series of stages; each stage will be defined by seven parameters that are described in Table \ref{disease_parameters}. Each stage is represented primarily by three normal distributions representing the duration of the stage and the infectivity and fatality of infected agents during the corresponding stage of infection. The decision to represent these values as distributions rather than constant values was due to the underlying variation that is often observed in agents presenting symptoms of infection.

\begin{table}[h]
	\centering
	\begin{tabular}{p{0.2\textwidth} | p{0.7\textwidth}}
		
		\rule{0pt}{3.2ex} \textbf{Parameter} & \textbf{Description} \\ [1ex]
		\hline		
		\rule{0pt}{3.2ex} $ name $ & Name of stage \\ [1ex]
		\rule{0pt}{3.2ex} $ d_{mean}, d_{std} $ & Normal distribution of stage's duration \\ [1ex]
		\rule{0pt}{3.2ex} $ i_{mean}, i_{std} $ & Normal distribution of stage's infectivity \\ [1ex]
		\rule{0pt}{3.2ex} $ f_{mean}, f_{std} $ & Normal distribution of stage's fatality \\ [1ex]
				
	\end{tabular}
	\caption{Disease stage parameters}
	\label{disease_parameters}
\end{table}

An infection represents an individual occurrence of a disease and is linked to a single member of the population; an infection will progress through the defined stages of the relevant disease to represent the progression of an infection over time. The transition between possible states of infection will not be a linear process, and using the model parameters it will be possible to assign probabilities to different transitions between infection stages. This design was selected to provide a more accurate representation of reality; it is common for a small number of cases to suffer different symptoms compared to other agents with the same infection, such as with severe or long COVID.

The model will support the implementation of six interventions, described in Table \ref{interventions_offered}. These interventions, besides vaccination, were selected due to their frequent use during the COVID-19 crisis; vaccination was also selected due to its proven effect on reducing disease transmission. Each of the possible interventions can be adjusted using a number of parameters - this enables the model to be used to study the effect of varying intensities and applications of interventions.

\begin{table}[h]
	\centering
	\begin{tabular}{p{0.2\textwidth} | p{0.7\textwidth}}
		
		\rule{0pt}{3.2ex} \textbf{Intervention} & \textbf{Description} \\ [1ex]
		\hline		
		\rule{0pt}{3.2ex} Hand washing & Agent wash hands ot prevent infections \\ [1ex]
		\rule{0pt}{3.2ex} Mask wearing & Agents wear a mask to prevent infection \\ [1ex]
		\rule{0pt}{3.2ex} Ventilation & Windows are opened in the environment \\ [1ex]
		\rule{0pt}{3.2ex} Distancing & Agents attempt to social distance \\ [1ex]
		\rule{0pt}{3.2ex} Testing & Testing performed on agents and any positive cases isolated \\ [1ex]
		\rule{0pt}{3.2ex} Vaccination & Agents vaccinated against the disease \\ [1ex]
		
	\end{tabular}
	\caption{Interventions supported by the model}
	\label{interventions_offered}
\end{table}

\section{Interface Mock-ups}
To understand what the solution's user interface may look like, a rapid iterative process was performed to develop initial interface prototypes. This process enabled various suggestions and ideas for the eventual interface to be explored prior to implementation and produced a series of interface mock-ups that are included in Appendix \ref{ui_mockups}.

In addition to producing a collection of user interface mock-ups, a number of rough visualizations were also sketched. These represented the visualizations and infographics that the solution planned to offer in order to provide an intuitive and informative view of a simulation's results. The produced visualization mock-ups are included in Appendix \ref{visualization_mockups}.

