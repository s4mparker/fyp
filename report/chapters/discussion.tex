\justifying

\chapter{Results \& Discussion}
\label{discussion}
This chapter will discuss the results of the evaluation process described in Chapter \ref{evaluation}. The later half of the chapter will explore whether the developed solution achieved the objectives discussed in Chapter \ref{project_objectives}.

\section{Final Solution}
\label{final_solution}
A selection of images showing the final solution in action are included in Appendix \ref{final_solution}.

\section{Testing}
\label{testing}
In order to test the final solution, the unit tests that were created during the solution's development were executed. In total 16 unit tests were created; ideally more would have been created however due to time constraints it was only possible to create unit tests for three of the modules. All 16 of the unit tests passed - this showed that the three components covered by the tests functioned as expected. Evidence of the passed tests, along with a full list of all the developed tests, is included in Figure \ref{passed_unit_tests} and Table \ref{defined_unit_tests} respectively.

In addition to performing unit testing on the solution, an informal process of functional testing was carried out to experiment with the solution and test the integration of the different components. It was during this process that en error was identified; there were occasions when running a simulation that despite the processing completing, the \texttt{Results} object appeared to get caught in a deadlock which essentially causes the simulation to never terminate. After closer inspection the error was narrowed down to the use of an inter-process queue which individual \texttt{Model} objects use to pass the \texttt{Results} object produced results. Reproducing the error was a challenge due to the use of low-level processes and despite issues to fix the problem using non-blocking methods and locks, due to time constraints the issue was never properly solved in the final solution.

\section{Case Study Results}
\label{case_study_results}
As discussed in Chapter \ref{casestudyintro}, a case study of a UK school was developed to understand both the applicability of the solution to user-defined environments and also to get a general idea of the accuracy that the solution is capable of.

The first time period to evaluate was September 2020, where the only interventions imposed were bubbles and social distancing. Unfortunately the produced solution doesn't offer bubbles as an intervention, so only social distancing can be enabled. For the purposes of the evaluation the protection offered by social distancing was assumed to be 12\%; this was based on research done on the effect different interventions had on COVID-19 transmission~\cite{intervention_effects}.

From the results displayed in Figure \ref{r1} and \ref{r2}, there's a noticeable difference in the model's predictions compared to what was expected. In both cases, the effect of the intervention doesn't appear to stop the spread of the virus, so fundamentally despite the varying behaviour shown the solution does line up, at least partially, with what was expected.

The second time period to evaluate was March 2020, where tri-weekly testing and face masks measures were implemented. For the purposes of the evaluation, face masks were assumed to reduce the likelihood of infection by 53\%; this was informed by a paper studying the effects different interventions had on reducing the spread of COVID-19~\cite{intervention_effects}. It was also assumed that lateral flow tests were used in UK schools with a success rate of 99.97\%~\cite{lateral_flows, lateral_flows_use}.

From the results displayed in Figures \ref{r3} and \ref{r4}, the two graphs appear to be quite similar. In both cases there are a relatively small number of new cases following the implementation of the interventions at the start of March. 

\section{Benchmarking Results}
\label{benchmarking_results}
The three outlined benchmarking experiments, discussed in Chapter \ref{benchmarkintro}, were performed to understand the factors that influence the solution's performance. 

The first test conducted aimed to investigate the effect that the selected execution mode had on the total execution time; the results from said experiment can be seen in Figure \ref{benchmarking_execution_mode}. The results received were as expected; the multiprocessing option performed significantly quicker, with the average execution taking 4.1s compared to the average serial execution time which averaged only 7.2s. This was a significant increase and represented a speed up of 43\%.

The second test aimed to investigate the effect that the length of the simulation had on the total execution time. To test this, a number of iterations were executed with varying simulation lengths; in addition to this both serial and multiprocessing execution modes were tested to understand how they impacted performance too. The results from this experiment is included in Figure \ref{benchmarking_simulation_length}; as expected the total execution time of a simulation increased as the number of days being simulated increased too. Another interesting observation was the effect the selected execution mode had one the execution time. While the serial execution time grew on average by a factor of 37\% with each additional day, the multiprocessing option only grew by a factor of 21\%. This was an impressive difference and while the multiprocessing option was expected to be more efficient, it wasn't quite by so much.

The third experiment aimed to understand the reproducibility of the solution's results. To test this reproducibility, a number of simulations were created and then executed twice in succession. The difference in results could then be calculated to determine how close the results were; the lower the difference the higher the reproducibility the solution was. As seen in the results, included in Figure \ref{benchmarking_reproducability}, the simulations that used serial execution provided identical results each time. In comparison the simulations that used multiprocessing failed to achieve the same, with approximately a 1\% difference between identical simulations. This was a peculiar result - after all both the simulations used identical parameters and random seeds. However after some thought a potential reasoning was identified: the computer's pseudo-random number generator will produce the same sequence of number each time, however due to the unpredictable nature of parallel processing, these random numbers could be delivered to different \texttt{Model} instances in a different order each time. This is the best explanation that could be found for this difference, and besides a couple of outlier cases, a 1\% different in total predicted cases is immaterial in the large picture so doesn't significantly affect the usage of the solution. This does however mean that the multiprocessing mode does not reliably output reproducible results.

\section{User Evaluation Results}
\label{user_evaluation_results}
In total, 18 responses were received from the user evaluation study; a breakdown of the results per question and user consent forms are included in Chapter \ref{questionnaire}.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\textwidth]{./figures/user_evaluation.png}
	\caption{User evaluation study results}
\end{figure}

Overall, the results were generally good and reflected a positive user experience in nearly all cases. Approximately 93\% of the questions were answered correctly, which demonstrates a relatively clear user pathway through the application which a non-technical user can access. Unfortunately there were a few cases where incorrect answers were entered by the study's participants.

The first piece of feedback was on Question 4, which asked users to select the option they would click on to add a new vaccination to the simulation. Only one participant gave an incorrect answer to this question; with them suggesting that they would select the 'Vaccination 1' entry in the planned interventions list box instead. This was a surprise as previously we thought the interactions were well labelled; however evidently this could be improved further to make the distinction between creating and updating interventions clearer. To achieve this, the labels surrounding the options could be more detailed with larger sub-headers, to really clarify to the user the difference between creating a new intervention and updating a pre-existing one.

Furthermore in Question 6, users are asked to identify the correct action to change the currently displayed graph. Only 61\% of the respondents selected the correct answer; with the other 39\% of participants selecting option 2 instead which pointed to the 'other' section of the application. Whilst a third of users getting the question wrong may be worry, this is primarily due to the sample size being so small that a wrong answer has a large impact on the question's overall results. It is however understand how users have come to this conclusion; the graph dropdown menu is quite small and hidden in the top left corner of the screen which makes it easy to miss if the user doesn't know what they're looking for. It then makes logical sense that they would assume it to be in the other section of the application. To remedy this, the graph selection functionality could be moved, potentially to its own tab, in the application's interface. This would make the interaction significantly more straightforward and harder to miss for a user who is unfamiliar with the solution.

In addition, a couple of free-form comments were received regarding the general presentation of the solution. 
\begin{itemize}
	\item One user suggested that more feedback was required from the application to make it clearer which tab was currently selected
	\item Another user praised the consistent use of buttons and font in the application
	\item Two comments were received that suggested more colour could be used in the application
\end{itemize}

These comments were really helpful to received, and definitely represent actionable items that could improved on in future generations. 

\section{Conclusion}
\label{overall_evaluation}
In conclusion, it is fair to say that the solution successfully achieves the objectives defined in Chapter \ref{project_objectives}. The tool enables a user to 'plug' in their custom-designed environments into the application to be used for complex pandemic simulations; the solution also offers a range of interactive features through the user interface that supports exploratory analysis. The opportunity to experiment with a range of interventions is a particularly impressive feature that really increases the potential applications of the solution to real-world problems. Finally the trivial implementation of Monte Carlo simulations is a real benefit for the tool; this enables more confidence in the results produced by the solution, a while this isn't perfect, it certain mitigates some of the inherently variability present in the process of disease transmission.

Despite its benefits, there are certainly a number of areas in which the solution could be improved. The error identified during the testing process limits the solution 



\section{Ideas for Future Work}
\label{future_work}
The pandemic simulation space is currently in its early stages and demand will only continue to rise as life returns to normality and policy-makers seek the most effective measures to prevent another surge in cases. Regarding the work conducted during this project, there's a number of pathways that could be taken in order to advance the produced simulation tool.

One interesting possibility for future work is the modelling of different forms of disease transmission. The current solution makes the rudimentary assumption that disease transmission is solely contact-based; this is not true in reality and often transmission occurs through multiple vectors, including surfaces and the air. This would make for an interesting expansion on the project and would add an interesting extra dimension to the analysis possible using the tool.

An alternative idea for future work is to further the application's interactive interface. As discussed in Chapter \ref{overall_evaluation}, the current solution offers a good level of interactivity, however at times can feel somewhat 'clunky'. This aspect of the solution could certainly be extended in the future and would significantly improve the user experience.
