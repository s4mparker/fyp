\chapter{Implementation}
\label{implementation}
As discussed in Chapter \ref{development_approach}, the implementation of the solution was split into two sections; the first phase of work involved the implementation of the pandemic model whilst the second phase focused on the implementation of the application interface and presentation. This chapter will be split into two sections to represent this division of work.

\section{Model}
\subsection{Summary}
The implementation of the model was primarily based around the execution of a Monte Carlo experiment. A Simulation class was created to manage the high-level process, whilst multiple instances of the Model class were used to represent the individual experiments within the experiment. Two supplementary classes were also used to manage the experiments: a Parameters class represented the model parameters whilst a Results class was used to manage the multiple sets of results produced by the different models. In total, this resulted in a relationship similar to the one shown in Figure \ref{simulation_relation}.

As discussed, an individual experiment was represented by the Model class. When instantiated, a Model object would construct its own Population and Disease objects based on the provided parameters. In addition an instance of the provided Environment is created to determine the rules of interaction between the agents. This results in a Model having a similar diagram to the one shown in Figure \ref{uml1}.

The execution of the individual Monte Carlo experiments was implemented in two different ways. One option was to process the Model objects serially; however due to the reasonably large number of Model objects that were likely to be used this raised concerns that the serial nature could significantly impact performance. To resolve this, a parallel option was also implemented that would attempt to run multiple experiments in parallel on the CPU. This topic will be covered more in Chapter \ref{performning_monte_carlo}.

\subsection{UML Diagram}
A UML diagram of the solution is shown in Figure \ref{uml1}; this is a simplified diagram.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.95\textwidth]{./figures/uml_simple.png}
	\caption{Summarised model UML diagram}
	\label{uml1}
\end{figure}

\subsection{Simulation}
A Simulation class was created to manage the simulation and the associated experiments that would be performed to forecast disease transmission. The contents of a Simulation object, in relation to the Model, Parameters and Results classes, is demonstrated in Figure \ref{simulation_relation}.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.95\textwidth]{./figures/simulation.png}
	\caption{High-level structure}
	\label{simulation_relation}
\end{figure}

\subsection{Model}
A Model class was created to represent a single experiment that would be performed. A Model object is self-encapsulating and has its own Population attribute and copy of the Environment, meaning hypothetically a Model could be created to represent a single model. 

\subsection{Parameters}
A Parameters class was used to encapsulate the parameters that would determine the simulation's execution; this enabled a single interface through which parameters could be updated. Each Model object is provided at initialization with a Parameters object; this determines the attributes of each model and is demonstrated by in Figure \ref{simulation_relation} by each model having the same set of parameters.

\subsection{Results}
A Results class was created to encapsulate the storage and management of the results produced by the different Model objects. Each Model object will be provided with a queue into which they can place their results; a Results object listens and remove items from the queue to condense the results produced by all the experiments into a single expected set of results.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.95\textwidth]{./figures/results.png}
	\caption{Results}
	\label{results_handling}
\end{figure}

\subsection{Population}
A Population class was created to represent a single Model object's population of agents. At initialization, based on the provided Parameters object, the Population object will be created with the pre-defined number and types of agents. As discussed in Chapter \ref{model_outline} agents will be have a number of attributes to represent their state; these attributes will be stored using arrays, with one array for each attribute. This means that for any agent $ i $, the data at index $ i $ of any of the attribute arrays will represent that agent's attribute values. This is shown in (b) in Figure \ref{agent_implementations}.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.95\textwidth]{./figures/comparison_agents.png}
	\caption{Possible options for agent implementation}
	\label{agent_implementations}
\end{figure}

The other option to represent the individual agents was by representing each agent in the population as an individual object. This approach was rejected for a number of reasons.

\begin{enumerate}
	\item There were concerns surrounding the performance of option (a), especially considering multiple models would be created, each with its own Population object. This, combined with Python's costly implementation of object-oriented concepts, had the potential to cause a significant performance bottleneck.
	\item Option (b) was also backed by Covasim, which represented agents as entries in arrays rather than as individual objects.
\end{enumerate}

Each of the agent attributes described in Table \ref{agent_attributes} was implemented as an array. In addition, a number of methods were written for the Population class; these included interact(a, b) which would simulate an interaction between agents a and b, randomly\_infect(n, d) which would be used at the start of the model's execution to infect the initial n agents, and test\_agents(e, c, i) that would test a certain proportion of the Population object's agents and isolate any positive cases.

\subsection{Disease \& Infection}
Two classes, Disease and Infection, were created to represent a disease and individual infections of agents. The Disease class stored the model's parameters (stages and transition matrix), whilst an Infection class stores the state of an agent's infection and progresses said state over time. To represent athe infection of a particular agent, a new Infection object is made using the infecting disease; this object is then assigned to the given agent's infection attribute in the appropriate Population object. With this, whenever an agent's state is updated (as time passes in the simulation), the infection's state will be advanced too. There are two options for an infection to conclude.

\begin{itemize}
	\item An infection reaches its conclusion (completes a state with no transitions), in which case the Infection object will be deleted and the agent's infection attribute cleared to reflect their newly un-infected status
	\item During an agent's state update, the infection causes the agent to pass. In this case the appropriate infection attribute will be cleared and the agent's active attribute set to False.
\end{itemize}

\subsection{Environment}
An important part of the solution's requirements is that users can define their own environments that they wish to model; to enable this an Environment interface was created. This interface requires any subclasses to implement a run() method which will be called during a simulation to calculate the interactions that will be simulated. Each Model instance will gain access to the user-defined environment using the model parameters; these will provide a class name and path which collectively can be used to import the user-defined environment. An instance of this custom class will then be instantiated and used to calculate the interactions that will be simulated. A short flowchart of this process is shown in Figure \ref{flowchart}. This enables the flexible use of user-defined environments.

\begin{figure}
	\centering
	\includegraphics[width=0.6\textwidth]{./figures/environment.png}
	\caption{Using a custom environment}
	\label{flowchart}
\end{figure}

\subsection{Interventions}
An interface called AbstractIntervention was created to model a generic intervention; this requires any subclasses to implement a constructor and action() method. To represent the six possible types of interventions, six subclasses of AbstractIntervention were created. Each one of these subclasses represented a particular type of intervention and implemented an appropiate action() method; this method would be called by the Model object when the appropriate day arose in order to action/implement the intervention. Whenever a Model is created, the supplied parameters will be parsed and the required intervention objects created and stored by the model.

This is a little inefficient as it essentially requires each Model object to maintain its own copies of the intervention objects; however this implementation made the process significantly simpler and meant that a Model object could encapsulate everything it needed to perform a simulation with no dependencies on other objects.

\subsection{Performing Monte Carlo Experiments}
\label{performning_monte_carlo}
As discussed, a Simulation object would manage multiple Model objects which represent the individual Monte Carlo experiments. The execution of these individual experiments could be done either serially or in parallel; both options offered their own benefits and drawbacks, which meant both were implemented in the final solution.

Running the individual Model objects serially was a simple approach to the problem, however due to the significant amount of processing that was required this raised performance concerns. To navigate this, Python's multiprocessing package was used to implement a rudimentary parallel solution. This essentially involved each Model running in its own designated process on the CPU; this enabled multiple experiments to run simultaneously and sped up execution by around two thirds in extreme cases. A similar approach was used to handle the collection of results; each Model running in its own process placed its results into an inter-process queue from where a stand-alone process retrieved the results and combined them with the rest of the results. A short diagram of this results handling process is included in Figure \ref{results_handling}.

In total implementing both of these options made sense, as it allowed both to be used depending on the user's requirements. The implementation of the multiple processes was certainly a challenge, however the final implementation functioned as required which was a big achievement for the project.

\section{User Interface \& Application}
\subsection{Summary}
The user interface was implemented primarily using Tkinter, with a number of wrapper widgets created based on their

\subsection{UML Diagram}
A UML diagram of the interface is shown in Figure \ref{uml2}; this is a simplified diagram.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{./figures/uml2_simple.png}
	\caption{Summarised interface UML diagram}
	\label{uml2}
\end{figure}

\subsection{Wrapper Widgets}
The majority of the user interface is implemented using Tkinter widgets, however a number of wrapper widgets were developed to add additional functionality to the pre-existing widgets. This aided the development of the interface significantly and abstracted away a significant amount of complexity behind tailor-made classes.

\subsection{Interface Components}
The interface is constructed from 9 components; each component is responsible for a different part of the interface and user interaction. The purpose of each interface component is described in Table \ref{interface_components}. 

\begin{table}[h]
	\centering
	\begin{tabular}{p{0.3\textwidth} | p{0.6\textwidth}}
		
		\rule{0pt}{3.2ex} \textbf{Component} & \textbf{Purpose} \\ [1ex]
		\hline
		\rule{0pt}{3.2ex} \texttt{ControlPanel} & Manage the interaction between the user and simulation controls. \\ [1ex]
		\rule{0pt}{3.2ex} \texttt{LogPanel} & Output any messages or errors to the user. \\ [1ex]
		\rule{0pt}{3.2ex} \texttt{DiseasePanel} & Manage the interaction between the user and the disease parameters. \\ [1ex]
		\rule{0pt}{3.2ex} \texttt{PopulationPanel} & Manage the interaction between the user and the population parameters. \\ [1ex]
		\rule{0pt}{3.2ex} \texttt{EnvironmentPanel} & Manage the interaction between the user and the environment parameters. \\ [1ex]
		\rule{0pt}{3.2ex} \texttt{InterventionPanel} & Manage the process of adding and updating planned interventions. \\ [1ex]
		\rule{0pt}{3.2ex} \texttt{OtherPanel} & Provide access to any additional parameters. \\ [1ex]
		\rule{0pt}{3.2ex} \texttt{InstructionPanel} & Display the application's instructions. \\ [1ex]
		\rule{0pt}{3.2ex} \texttt{MenuBar} & Provide an application-level menu bar. \\ [1ex]
								
	\end{tabular}
	\caption{Interface components}
	\label{interface_components}
\end{table}

The components described will be managed under an \texttt{Application} class, which itself inherits from a Tkinter top-level window. A parent-child hierarchy will be used to structure the interface components, with individual sub0widgets of each other 9 component stored as component children.

\subsection{Constructing Visualizations}
To construct the visualizations that would be displayed within the user interface, a GraphBuilder class was created. Using the application's MenuBar object, a number of graphs could be selected to become the active graph for the GraphBuilder object. Finally whenever the 




