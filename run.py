""" External """
import sys

""" Internal """
from simulation import Simulation, Parameters

""" 

This file will run a single model with some given parameters and output the results to an external file

Usage:
python3 run.py <parameters> [-d <destination>]

"""

# Parameters().json_export('custom/data.json')

args = sys.argv
args.pop(0)

if len(args) == 0: 
    print('Usage: python3 run.py <parameters> [-d <destination>]')
    sys.exit(0)

parameters = Parameters(args[0])
simulation = Simulation()
simulation.set_parameters(parameters=parameters)
simulation.run()
