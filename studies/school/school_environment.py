from simulation import Environment
import numpy as np

class School(Environment):

		def setup(self):
			
			# split the student into 5 year groups
			students = np.argwhere(self.pop.name == 'student').flatten()
			self.pop.year = np.full_like(self.pop.name, -1, dtype=int)
			self.pop.year[students] = np.random.randint(0, 5, size=students.shape)

			# create the arrays to contain the agent's lessons and friendship groups
			self.pop.s1 = np.full_like(self.pop.space, 0)
			self.pop.s2 = np.full_like(self.pop.space, 0)
			self.pop.s3 = np.full_like(self.pop.space, 0)
			self.pop.s4 = np.full_like(self.pop.space, 0)
			self.pop.s5 = np.full_like(self.pop.space, 0)
			self.pop.f = np.full_like(self.pop.space, 0)

			# assign each agent to a class
			for y in range(5):
				students = np.argwhere(self.pop.year == y).flatten()
				self.pop.s1[students] = np.random.randint(self.parameters['classes']*y, self.parameters['classes']*(y+1))
				self.pop.s2[students] = np.random.randint(self.parameters['classes']*y, self.parameters['classes']*(y+1))
				self.pop.s3[students] = np.random.randint(self.parameters['classes']*y, self.parameters['classes']*(y+1))
				self.pop.s4[students] = np.random.randint(self.parameters['classes']*y, self.parameters['classes']*(y+1))
				self.pop.s5[students] = np.random.randint(self.parameters['classes']*y, self.parameters['classes']*(y+1))
				self.pop.f[students] = np.random.randint(self.parameters['friends']*y, self.parameters['friends']*(y+1))

			# assign the teachers to classes
			teachers = np.argwhere(self.pop.name == 'teacher').flatten()
			partitions = np.array_split(teachers, 5 * self.parameters['classes'])
			for i in range(5 * self.parameters['classes']):
				for t in partitions[i]:
					self.pop.s1[t] = i
					self.pop.s2[t] = i
					self.pop.s3[t] = i
					self.pop.s4[t] = i
					self.pop.s5[t] = i
					self.pop.f[t] = -1

		def run(self):
			# class one
			self.pop.space = self.pop.s1
			self.class_interactions()
			# class two
			self.pop.space = self.pop.s2
			self.class_interactions()
			# breaktime
			self.pop.space = self.pop.f
			self.random_interactions()
			# class three
			self.pop.space = self.pop.s3
			self.class_interactions()
			# lunchtime
			self.pop.space = self.pop.f
			self.random_interactions()
			# class four
			self.pop.space = self.pop.s4
			self.class_interactions()
			# class five
			self.pop.space = self.pop.s5
			self.class_interactions()

		# each teacher > interact with every student in their class
		def class_interactions(self):		
			# calculate the teacher spaces	
			teacher_spaces = np.unique(self.pop.space[np.argwhere(self.pop.name == 'teacher').flatten()])
			for space in teacher_spaces:
				# calculate the teachers and students in a space
				all_in_space = np.argwhere(self.pop.space == space).flatten()
				teachers_in_space = []
				student_in_space = []
				for agent in all_in_space:
					if self.pop.name[agent] == 'teacher': 
						teachers_in_space.append(agent)
					else: 
						student_in_space.append(agent)
				
				# interact the agents
				for teacher in teachers_in_space:
					for student in student_in_space:
						self.interact(teacher, student)
