""" External """
import multiprocessing as mp
import numpy as np
import time
import queue

""" Internal """
from .components import ModelResult

""" Packaging """
__all__ = ['Results']

class Results:

    def __init__(self):

        self.states = mp.Manager().dict()
        self.active = mp.Manager().dict()
        self.isolation = mp.Manager().dict()
        self.entries = mp.Manager().dict()
        self.models = []
        self.counter = 0

        self.lock = mp.Manager().Lock()
        self.queue = mp.Manager().Queue()
        self.subprocess = None

    def add_models(self, models):

        self.models = models

    def clear_models(self):

        self.models = []

    def clear_results(self):

        for attr in [self.states, self.active, self.isolation, self.entries]: attr = attr.clear()

    def add(self, day, **kwargs):

        if any([getattr(self, kwarg, None) == None for kwarg in kwargs]) : raise ValueError('error')
        if day not in self.entries.keys():
            for kwarg in kwargs:
                attr = getattr(self, kwarg)
                attr[day] = kwargs.get(kwarg)
            self.entries[day] = 1
        else:
            for kwarg in kwargs:
                attr = getattr(self, kwarg)
                val = kwargs.get(kwarg)

                updated_data = {}
                keyset = set(list(val.keys()) + list(attr[day].keys()))
                for key in keyset:
                    updated_data[key] = (attr[day].get(key, 0) * self.entries[day] + val.get(key, 0)) / (self.entries[day] + 1)

                attr[day] = updated_data
            self.entries[day] = self.entries[day] + 1
    
    def get(self, value):

        return_val = getattr(self, value, None)
        return return_val

    def start(self):

        self.subprocess = ResultsHandler(results=self, queue=self.queue, delay=0)
        self.subprocess.start()

    def stop(self):

        self.subprocess.join(target=len(self.models))
        self.subprocess = None

    def get_queue(self):

        return self.queue

    def get_interventions(self):

        if len(self.models) > 0 : return self.models[0].get_all_interventions()
        else : return {}

class ResultsHandler(mp.Process):

    def __init__(self, results, queue, delay=0):

        super().__init__()

        self.results = results
        (self.r, self.s) = mp.Pipe(duplex=False)
        self.queue = queue
        self.delay = delay

    def run(self):

        while not self.r.poll(self.delay):
            self.get()

    def get(self):

        # print(f'Getting from queue: {self.queue.qsize()} estimated')

        result = None
        try : result = self.queue.get(False)
        except queue.Empty: 
            return

        (day, data) = result.get()
        summaries = self.produce(data)
        self.results.add(day, **summaries)

    def produce(self, data):

        # Active Status
        active = np.array(data['Active'], dtype=str)
        (uniques, counts) = np.unique(active, return_counts=True)
        active = {uniques[i]: counts[i] for i in range(len(uniques))}

        # States
        states = np.array(data['Infection'], dtype=str)
        states[np.argwhere(np.array(data['Active']) == False).flatten()] = 'passed'
        states[np.argwhere(states == 'None').flatten()] = 'sus'
        (uniques, counts) = np.unique(states, return_counts=True)
        states = {uniques[u]: counts[u] for u in range(len(uniques))}

        # Isolation
        isolation = np.array(data['Isolation'], dtype=int)
        isolating = len(np.argwhere(isolation != 0).flatten())
        nonisolating = len(np.argwhere(isolation == 0).flatten())
        isolation = {'isolating': isolating, 'non-isolating': nonisolating}

        return {'active': active, 'states': states, 'isolation': isolation}

    def join(self, target):

        self.s.send('stop')
        self.flush(target)
        super().join()
        # self.close()
        
    def flush(self, target):

        limit = 10000

        while True:
            self.get()
            limit -= 1
            counts = [count for count in self.results.entries.values()]
            check = [count == target for count in counts]
            if (self.queue.empty() or self.queue.qsize() == 0) or all(check) or limit <= 0:
                break

                # if limit == 0: 
                #     print('Escaped')
                #     break
                # print('A')

                # try : self.queue.get(block=False, timeout=1)
                # except queue.Empty : break
