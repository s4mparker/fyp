""" External """
import numpy as np
import multiprocessing as mp
import threading as th
import time

""" Internal """
from . import Model, MissingComponentError, Results

""" Packaging """
__all__ = ['Simulation']

def batch(instance, days):
    for i in range(days): 
        instance.step()
    return instance

class Simulation:

    def __init__(self, callback=None):

        self.callback   = callback
        self.compiled   = False

        self.models     = []
        self.parameters = None

        # Create the results handler
        self.results = Results()
        
    def set_parameters(self, parameters): 
        self.parameters = parameters

    def set_callback(self, callback):
        self.callback = callback

    def run(self):

        # Validate the parameters
        if self.parameters is None : raise MissingComponentError('missing parameters from simulation')
        if not self.parameters.validate() : raise ValueError('invalid parameters')

        # Set the random seed for the simulation
        np.random.seed(self.parameters.seed)

        # Get the message passing queue
        q = self.results.get_queue()
        # Create the induvidual models
        for n in range(self.parameters.models) : self.models.append(Model(self.parameters, q))
        # Update the results handler
        self.results.add_models(self.models)

        # Output progress message (if required)
        start = time.time()

        # Begin the results handler to gather results from each model
        self.results.start()

        # Run using the provided mode of execution
        if self.parameters.mode == 'serial':
            for m in self.models : batch(m, self.parameters.days)
            # for n in range(self.parameters.days): 
            #     for m in self.models:
            #         m.step()

        if self.parameters.mode == 'multiprocessing':
            with mp.Pool() as pool:
                iterable = ((model, self.parameters.days) for model in self.models)
                pool.starmap(batch, iterable)

        # # Output progress message (if required)
        if self.parameters.verbose >= 1 : print(f'Simulation completed in {format(round(time.time() - start, 5), "f")}s')

        # End the results collector and flush out any remaining models
        self.results.stop()

        if self.callback is not None: self.callback()

    def reset(self):

        # Remove the connected models
        self.models = []

        # Delete the results handler
        self.results.clear_results()
        self.results.clear_models()

    def get_results(self):

        return self.results

