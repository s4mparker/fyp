""" External """
import numpy as np
import pandas as pd
import multiprocessing as mp
import time

""" Internal """
from . import Population, Disease, TestingIntervention,VaccinationIntervention, HandWashingIntervention, MasksIntervention, VentilationIntervention, DistancingIntervention
from . import InvalidSimulationParameters, InvalidSimulationRuntime

""" Packaging """
__all__ = ['Model', 'ModelResult']

class ModelResult:

    def __init__(self, day, data):

        self.day = day
        self.data = data

    def __str__(self):

        return f'RESULT: {self.day}'

    def get(self):

        return self.day, self.data

class Model:

    def __init__(self, parameters, results):

        # Generate a unique model ID
        self.id = '.'.join([str(v) for v in np.random.choice(range(10), size=3, replace=True)])

        # Save a reference to the results queue
        self.results = results
        
        # Save verbose configuration
        self.verbose = parameters.verbose

        # Day in model
        self.day = 0

        # Setup population & infect the initial agents
        self.population = Population(parameters.agents)
        disease = Disease(parameters.stages, parameters.transitions)
        self.population.randomly_infect(parameters.initial, disease)

        # Setup environment
        self.environment = self.setup_environment(parameters=parameters)
        self.environment.setup()

        # Setup interventions
        self.interventions = self.setup_interventions(parameters=parameters)

        # Setup testing
        self.testing = None

        # Output creation message if required
        if self.verbose >= 2 : print(f'Model created ({self.id})')

    def step(self):

        # Check for any new interventions
        self.poll_interventions()

        # Update agent states
        self.population += 1

        # Test the agents (if required)
        if self.testing is not None and self.day % self.testing['frequency'] == 0:
            self.population.test_agents(self.testing['efficiency'], self.testing['coverage'], self.testing['isolation'])

        # Run the environment
        self.environment.run()

        # Update the model's day
        self.day = self.day + 1

        # Output step message if required
        if self.verbose >= 2 : print(f'Model stepped {self.id} ({self.day})')

        # Record the results
        self.record()

    def record(self):

        # Extract the population's data
        full_data = self.population.output()

        # Output (if required)
        if self.verbose >= 3 :
            print('\n' + '-' * 42 + ' Day ' + str(self.day) + ' (' + str(self.id) + ')' + '-' * 42)
            print(full_data)

        # Store the results
        entry = ModelResult(self.day, full_data)
        self.results.put(entry)

    def poll_interventions(self):

        list = self.interventions.get(self.day, [])
        for intervention in list : intervention.action()

    def get_all_interventions(self):

        return list(self.interventions.keys())

    def setup_environment(self, parameters):
        file = __import__(parameters.path, fromlist=[parameters.name])
        env = getattr(file, parameters.name)
        env = env(population=self.population, spaces=parameters.spaces, parameters=parameters.env_parameters)
        return env

    def setup_interventions(self, parameters):

        mapping = {
            'testing': TestingIntervention,
            'vaccination': VaccinationIntervention,
            'handwashing': HandWashingIntervention,
            'masks': MasksIntervention,
            'ventilation': VentilationIntervention,
            'distancing': DistancingIntervention
        }

        interventions = {}
        for intervention in parameters.interventions:

            # Extract the intervention's parameters from the configuration file
            itype = intervention['type']
            day = intervention['day']
            values = intervention['parameters']

            # Check the intervention is valid and if so create the intervention
            intervention_type = mapping.get(itype, None)
            if intervention_type is None : raise InvalidSimulationParameters(f'(model) unrecognised intervention type encountered ({type})')
            selected = intervention_type(model=self, **values)

            # Store the intervention
            if day in interventions.keys() : interventions[day].append(selected)
            else : interventions[day] = [selected]

        for invs in interventions.values():
            types = [type(var) for var in invs]
            if len(invs) != len(set(invs)) : raise InvalidSimulationParameters('(model) encountered two identical types of intervention for the same day')

        return interventions
