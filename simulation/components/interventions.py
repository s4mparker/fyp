""" External """
import numpy as np

""" Internala """
from . import InvalidSimulationParameters

""" Packaging """
__all__ = [
    'TestingIntervention', 
    'VaccinationIntervention',
    'HandWashingIntervention',
    'MasksIntervention',
    'VentilationIntervention',
    'DistancingIntervention'
]

class TestingIntervention:

    __name__ = 'Testing'
    short = 'T'

    def __init__(self, model, **parameters):

        self.model = model
        self.enabled = parameters.get('enabled', None)
        self.efficiency = parameters.get('efficiency', None)
        self.frequency = parameters.get('frequency', None)
        self.coverage = parameters.get('coverage', None)
        self.isolation = parameters.get('isolation', None)

        if any([var is None for var in [self.model, self.enabled, self.efficiency, self.frequency, self.coverage, self.isolation]]):
            raise InvalidSimulationParameters(f'PARAMETERS ERROR: Encountered an missing intervention parameter')

    def action(self):

        if self.enabled : self.model.testing = {'efficiency': self.efficiency, 'frequency': self.frequency, 'coverage': self.coverage, 'isolation': self.isolation}
        else: self.model.testing = None

class VaccinationIntervention:

    __name__ = 'Vaccination'
    short = 'V'

    def __init__(self, model, **parameters):

        self.model = model
        self.effect = parameters.get('effect', None)

        if any([var is None for var in [self.model, self.effect]]):
            raise InvalidSimulationParameters(f'PARAMETERS ERROR: Encountered an missing intervention parameter')

    def action(self):

        pop = self.model.population
        pop.vaccination = np.full(pop.size, self.effect)

class HandWashingIntervention:

    __name__ = 'Hand Washing'
    short = 'Hw'

    def __init__(self, model, **parameters):
        self.model = model
        self.mean = parameters.get('mean', None)
        self.std = parameters.get('std', None)

        if any([var is None for var in [self.model, self.mean, self.std]]):
            raise InvalidSimulationParameters(f'PARAMETERS ERROR: Encountered an missing intervention parameter')

    def action(self):

        pop = self.model.population
        pop.handwashing = np.clip(np.random.normal(loc=self.mean, scale=self.std, size=pop.size), 0, 1)

class MasksIntervention:

    __name__ = 'Masks'
    short = 'M'

    def __init__(self, model, **parameters):
        self.model = model
        self.mean = parameters.get('mean', None)
        self.std = parameters.get('std', None)

        if any([var is None for var in [self.model, self.mean, self.std]]):
            raise InvalidSimulationParameters(f'PARAMETERS ERROR: Encountered an missing intervention parameter')

    def action(self):

        pop = self.model.population
        pop.masks = np.clip(np.random.normal(loc=self.mean, scale=self.std, size=pop.size), 0, 1)

class VentilationIntervention:

    __name__ = 'Ventilation'
    short = 'Ve'

    def __init__(self, model, **parameters):
        self.model = model
        self.mean = parameters.get('mean', None)
        self.std = parameters.get('std', None)

        if any([var is None for var in [self.model, self.mean, self.std]]):
            raise InvalidSimulationParameters(f'PARAMETERS ERROR: Encountered an missing intervention parameter')

    def action(self):

        pop = self.model.population
        pop.ventilation = np.clip(np.random.normal(loc=self.mean, scale=self.std, size=pop.size), 0, 1)

class DistancingIntervention:

    __name__ = 'Distancing'
    short = 'D'

    def __init__(self, model, **parameters):
        self.model = model
        self.mean = parameters.get('mean', None)
        self.std = parameters.get('std', None)

        if any([var is None for var in [self.model, self.mean, self.std]]):
            raise InvalidSimulationParameters(f'PARAMETERS ERROR: Encountered an missing intervention parameter')

    def action(self):

        pop = self.model.population
        pop.distancing = np.clip(np.random.normal(loc=self.mean, scale=self.std, size=pop.size), 0, 1)