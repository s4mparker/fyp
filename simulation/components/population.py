""" External """
import numpy as np
import pandas as pd
import logging

""" Internal """
from . import Infection, InvalidSimulationParameters, InvalidSimulationRuntime

""" Packaging """
__all__ = ['Population']

class Population:

    def __init__(self, agents):

        self.validate(agents=agents)
        self.size               = 0

        # Initialize empty states arrays
        self.active             = np.empty(0, dtype=bool)
        self.name               = np.empty(0, dtype=str)
        self.space              = np.empty(0, dtype=int)
        self.infection          = np.empty(0, dtype=Infection)
        self.vulnerability      = np.empty(0, dtype=float)

        # Update the state arrays to represent the agents being added
        for name, agent in agents.items():

            q = agent['quantity']
            self.size           += q
            self.active         = np.concatenate([self.active, np.full(q, True)])
            self.name           = np.concatenate([self.name, np.full(q, name)])
            self.infection      = np.concatenate([self.infection, np.full(q, None)])
            self.vulnerability  = np.concatenate([self.vulnerability, np.random.normal(loc=agent['mean'], scale=agent['std'], size=q)])

        # Clip agent vulnerabilites to be in range [0-1]
        self.vulnerability      = np.clip(self.vulnerability, 0, 1)

        # Create the final (and empty) state arrays for the population
        self.space              = np.full(self.size, None)
        self.isolation          = np.full(self.size, 0, dtype=int)
        self.bubbles            = np.full(self.size, 0, dtype=int)
        self.vaccination        = np.full(self.size, 0, dtype=float)
        self.handwashing        = np.full(self.size, 0, dtype=float)
        self.masks              = np.full(self.size, 0, dtype=float)
        self.ventilation        = np.full(self.size, 0, dtype=float)
        self.distancing        = np.full(self.size, 0, dtype=float)

    @staticmethod
    def validate(agents):

        if agents is None or agents == {} : raise InvalidSimulationParameters('(population) invalid agents')
        if len(agents) < 1 :raise InvalidSimulationParameters('(population) no agents defined')

        for name, params in agents.items():
            if len(name) < 1: 
                raise InvalidSimulationParameters('(population) invalid agent name')
            if any([var not in params.keys() for var in ['quantity', 'mean', 'std']]):
                raise InvalidSimulationParameters('(population) agent found with missing parameters')
            if len(params.keys()) != 3: 
                raise InvalidSimulationParameters('(population) agent found with unknown parameter')
            if type(params['quantity']) is not int:
                raise InvalidSimulationParameters('(population) agent found with invalid quantity')
            if type(params['mean']) not in [int, float]:
                raise InvalidSimulationParameters('(population) agent found with invalid mean')
            if type(params['std']) not in [int, float]:
                raise InvalidSimulationParameters('(population) agent found with invalid std')
            if params['quantity'] < 0:
                raise InvalidSimulationParameters('(population) agent found with invalid quantity')
            if params['mean'] < 0 or params['mean'] > 1:
                raise InvalidSimulationParameters('(population) agent found with invalid mean')
            if params['std'] < 0 or params['std'] > 1:
                raise InvalidSimulationParameters('(population) agent found with invalid std')

    def __add__(self, value):

        """ Increment a population's states """

        # Update the diseases' states

        infected_indices = np.argwhere(self.infection != None).flatten()
        infections = self.get_agents(infected_indices, active=True)
        self.infection[infections] += 1

        # Update isolating periods

        isolating_indices = np.argwhere(self.isolation > 0).flatten()
        isolations = self.get_agents(isolating_indices, active=True)
        self.isolation[isolations] -= 1

        # Update any passed agents

        infected_indices = np.argwhere(self.infection != None).flatten()
        infections = self.get_agents(infected_indices, active=True)

        for agent in infections:
            result = self.infection[agent].survive(self.vulnerability[agent] - self.vaccination[agent])
            self.active[agent] = result
            if not result : logging.info('Agent Passed')

        return self

    def output(self):

        return pd.DataFrame(data=[  self.name, self.active, self.space, self.vulnerability, 
                                    self.infection, self.isolation, self.bubbles, self.vaccination, 
                                    self.handwashing, self.masks, self.ventilation, self.distancing ], index=[
                                    'Name', 'Active', 'Spaces', 'Vuln', 'Infection', 
                                    'Isolation', 'Bubbles', 'Vaccination', 'Hands', 
                                    'Masks', 'Ventilation', 'Distancing']).transpose()

    def randomly_infect(self, n, disease):
        """ Randomly infect n agents with the provided disease """

        active_agents = self.get_agents()
        if n > len(active_agents) or n < 0 : raise InvalidSimulationRuntime('(population) cannot infected more agents than in the population')
        for i in np.random.choice(active_agents, replace=False, size=n) : self.infection[i] = Infection(disease)

    def test_agents(self, efficiency, coverage, isolation):

        if type(efficiency) not in [float, int] or type(coverage) not in [float, int] or type(isolation) is not int:
            raise InvalidSimulationRuntime('(population) invalid testing parameters')
        if any([var < 0 for var in [efficiency, coverage, isolation]]):
            raise InvalidSimulationRuntime('(population) invalid testing parameters')

        agents = np.random.choice(self.get_agents(active=False), replace=False, size=int(self.size * coverage))
        non_isolating = np.argwhere(self.isolation == 0).flatten()
        infected = np.argwhere(self.infection != None).flatten()
        agents = self.get_agents(agents, non_isolating, infected, active=True)

        rand_vals = np.random.random(len(agents))
        threshold = np.full(len(agents), efficiency)
        positives = agents[np.argwhere(rand_vals <= threshold).flatten()]
        self.isolation[positives] = isolation

    def get_agents(self, *lists, active=True):

        if active : result = np.argwhere(self.active == True).flatten()
        else : result = np.arange(self.size)
        for list in lists : result = np.intersect1d(result, list)
        return result

    def interact(self, a, b, bidirectional=True):

        if any([var < 0 or var >= len(self.active) for var in [a, b]]):
            raise InvalidSimulationRuntime('(population) interaction with an invalid agent')
        if a == b:
            raise InvalidSimulationRuntime('(population) agent cannot interact with itself')
        if not self.active[a] or not self.active[b]: 
            return
        
        if self.infection[a] != None and self.infection[b] == None:
            a_infectivity = self.infection[a].get_infectivity()
            b_threshold = (1 - np.random.rand()) + self.vaccination[b] + self.handwashing[b] + self.masks[b] + self.ventilation[b] + (self.distancing[a] + self.distancing[b])/2

            if b_threshold <= a_infectivity:
                logging.info(f'Infection between {a} -> {b}')
                self.infection[b] = Infection(self.infection[a].get_disease())

        if bidirectional : self.interact(b, a, bidirectional=False)

    def infect(self, infector, infected):

        if self.infection[infector] is None:
            raise InvalidSimulationRuntime('(population) uninfected agent attempting to infect another agent')
        if self.infection[infected] is not None:
            raise InvalidSimulationRuntime('(population) attempting to infect an already infected agent')
        
        self.infection[infected] = Infection(self.infection[infector].get_disease())

