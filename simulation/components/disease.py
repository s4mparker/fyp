""" External """
import numpy as np
from copy import copy

""" Internal """
from . import InvalidSimulationParameters, InvalidSimulationRuntime

""" Packaging """
__all__ = ['Disease', 'Infection']

class Disease:

    def __init__(self, stages, transitions):

        self.validate(stages=stages, transitions=transitions)
        self.stages = stages
        self.transitions = transitions
    
    @staticmethod
    def validate(stages, transitions):

        # Check basic conditions
        if stages is None or stages == {} : raise InvalidSimulationParameters('(disease) invalid stages')
        if transitions is None or transitions == {} : raise InvalidSimulationParameters('(disease) invalid transitions')
        if len(stages) < 1 : raise InvalidSimulationParameters('(disease) no stages defined')
        if len(transitions) < 1 : raise InvalidSimulationParameters('(disease) no transitions defined')

        # Check the supplied stages
        for name, params in stages.items():
            if type(params) is not dict:
                raise InvalidSimulationParameters('(disease) invalid stage found')
            if len(name) < 1: 
                raise InvalidSimulationParameters('(disease) invalid stage name')
            if any([var not in params.keys() for var in ['length', 'infectivity', 'fatality']]):
                raise InvalidSimulationParameters('(disease) stage found with missing parameters')
            if len(params.keys()) != 3: 
                raise InvalidSimulationParameters('(disease) stage found with unknown parameter')
            for values in params.values():
                if type(values) is not dict:
                    raise InvalidSimulationParameters('(disease) invalid stage parameters found')
                if any([var not in values.keys() for var in ['mean', 'std']]):
                    raise InvalidSimulationParameters('(disease) stage parameter found with missing value')
                if len(values.keys()) != 2:
                    raise InvalidSimulationParameters('(disease) stage parameter found with unknown value')
            if type(params['length']['mean']) not in [int, float] or params['length']['mean'] < 0:
                raise InvalidSimulationParameters('(disease) stage found with invalid length mean')
            if type(params['length']['std']) not in [int, float] or params['length']['std'] < 0:
                raise InvalidSimulationParameters('(disease) stage found with invalid length std')
            if type(params['infectivity']['mean']) not in [int, float] or params['infectivity']['mean'] < 0 or params['infectivity']['mean'] > 1:
                raise InvalidSimulationParameters('(disease) stage found with invalid infectivity mean')
            if type(params['infectivity']['std']) not in [int, float] or params['infectivity']['std'] < 0:
                raise InvalidSimulationParameters('(disease) stage found with invalid infectivity std')
            if type(params['fatality']['mean']) not in [int, float] or params['fatality']['mean'] < 0 or params['fatality']['mean'] > 1:
                raise InvalidSimulationParameters('(disease) stage found with invalid fatality mean')
            if type(params['fatality']['std']) not in [int, float] or params['fatality']['std'] < 0:
                raise InvalidSimulationParameters('(disease) stage found with invalid fatality std')

        # Check the supplied transitions
        if 'initial' not in transitions.keys():
            raise InvalidSimulationParameters('(disease) missing initial transitions')
        for name, options in transitions.items():
            if len(name) < 1:
                raise InvalidSimulationParameters('(disease) invalid transition name')
            if len(options.keys()) < 1:
                raise InvalidSimulationParameters('(disease) uncomplete transition found')
            if sum([var for var in options.values()]) != 1:
                raise InvalidSimulationParameters('(disease) invalid transition found - probabilities must sum to 1')
            for destination in options.keys():
                if destination not in stages.keys():
                    raise InvalidSimulationParameters('(disease) transition found to unrecognized stage')

    def progress(self, current='initial'):

        possibilities = list(self.transitions.get(current, {}).keys())
        weights = list(self.transitions.get(current, {}).values())
        if len(possibilities) == 0 : return None

        current = np.random.choice(possibilities, p=weights)
        length = int(np.random.normal(loc=self.stages[current]['length']['mean'], scale=self.stages[current]['length']['std']))
        infectivity = np.clip(np.random.normal(loc=self.stages[current]['infectivity']['mean'], scale=self.stages[current]['infectivity']['std']), 0, 1)
        fatality = np.clip(np.random.normal(loc=self.stages[current]['fatality']['mean'], scale=self.stages[current]['fatality']['std']), 0, 1)
        return (current, length, infectivity, fatality)
        
    def get(self):

        return copy(self)

class Infection:

    def __init__(self, disease):
        """ Create a new Infection 
        
        Parameters:
        disease     (Disease) : the disease which causes the infection

        """

        self.validate(disease=disease)
        self.elapsed = 0
        self.disease = copy(disease)
        sample = self.disease.progress()
        (self.stage, self.remaining, self.infectivity, self.fatality) = sample

    @staticmethod
    def validate(disease):
        if not isinstance(disease, Disease) : raise InvalidSimulationRuntime('(infection) attempted a new infection with an invalid disease')

    def __add__(self, value):
        """ Update an Infection's state 
        
        Parameters:
        value   (int) : The number of cycles to process

        Returns:
        Infection   : The updated Infection

        """

        for i in range(value):

            self.elapsed += 1
            self.remaining -= 1

            if self.remaining <= -1:
                sample = self.disease.progress(current=self.stage)
                if sample is None : return None
                else: (self.stage, self.remaining, self.infectivity, self.fatality) = sample

        return self

    def __str__(self):
        """ Return the string representation of an Infection 
        
        Returns:
        str : String representation

        """

        return f'{self.stage}'

    def get_infectivity(self):
        """ Return the Infection's current infectivity value 
        
        Returns:
        float   : The Infection's current infectivity

        """

        return self.infectivity

    def survive(self, vulnerability):
        """ Determine whether the host of an infection survives 

        Parameters:
        vulnerability   (int) : The host's infectivity
        
        Returns:
        bool    : Whether the host survives or not

        """

        fatality = self.fatality
        rand_val = max(np.random.random() - vulnerability, 0)
        return rand_val >= fatality

    def get_disease(self):
        """ Get a copy of the infection's disease 
        
        Returns:
        Disease : The Infection's disease

        """
        
        return self.disease.get()
