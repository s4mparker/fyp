""" External """
import numpy as np
import itertools
import random

""" Internal """
from . import Population

""" Packaging """
__all__ = ['Environment']

class Environment:

    def __init__(self, population, spaces, parameters):

        self.pop = population
        self.spaces = spaces
        self.parameters = parameters

    def run(self):

        raise NotImplementedError('Must be implemented by a child class')

    def setup(self):

        pass

    def clear_spaces(self):

        self.pop.space = np.full(self.pop.size, None)

    def interact(self, a, b): 
        
        self.pop.interact(a, b)

    def uniform(self):

        agents = np.argwhere(self.pop.space != None).flatten()

        for space in np.unique(self.pop.space[agents]):
            members = agents[np.argwhere(self.pop.space[agents] == space).flatten()]
            # print(f'Space {space} -> {members}')
            combinations = list(itertools.combinations(members, 2))

            for (a, b) in combinations:
                self.interact(a, b)

    def random_interactions(self):

        agents = np.argwhere(self.pop.space != None).flatten()
        for space in np.unique(self.pop.space[agents]):
            members = np.argwhere(self.pop.space == space).flatten()
            combinations = list(itertools.combinations(members, 2))
            choices = random.choices(combinations, k=int(len(combinations)*self.parameters['random']))
            for (a, b) in choices:
                self.interact(a, b)

    def kNN(self):

        pass

    def binomal(self):

        agents = np.argwhere(self.pop.space != None).flatten()

        for space in np.unique(self.pop.space[agents]):
            members = np.argwhere(self.pop.space[agents] == space).flatten()
            infected = members[np.argwhere(self.pop.infection[members] != None).flatten()]
            clear = members[np.argwhere(self.pop.infection[members] == None).flatten()]

            infection_probs = np.array([self.pop.infection[agent].get_infectivity() for agent in infected])
            i_len = len(infection_probs)
            
            for agent in clear:
                r = self.pop.vaccination[agent] + self.pop.static_offset[agent] + self.pop.dynamic_offset[agent]

                resistance = np.full(i_len, 1 + r)
                result = np.random.rand() <= np.prod(np.subtract(resistance, infection_probs))

                if not result:
                    infector = np.random.choice(infected)
                    self.pop.infect(infector, agent)

    def combination(self):

        infected = np.argwhere(self.pop.infection != None).flatten()
        active = np.argwhere(self.pop.active == True).flatten()

        if len(infected) > len(active) / 2 : self.binomal()
        else: self.uniform() 

    def combination2(self):

        agents = np.argwhere(self.pop.space != None).flatten()

        for space in np.unique(self.pop.space[agents]):
            members = np.argwhere(self.pop.space[agents] == space).flatten()
            infected = members[np.argwhere(self.pop.infection[members] != None).flatten()]
            clear = members[np.argwhere(self.pop.infection[members] == None).flatten()]

            infection_probs = np.array([self.pop.infection[agent].get_infectivity() for agent in infected])

            if len(infection_probs) > 1 and np.std(infection_probs) < 0.25:
                avg_inf = np.average(infection_probs)

                for agent in clear:
                    r = self.pop.vaccination[agent] + self.pop.static_offset[agent] + self.pop.dynamic_offset[agent]
                    result = np.random.rand() <= (r-avg_inf)**len(infection_probs)
                    if not result:
                        infector = np.random.choice(infected)
                        self.pop.infect(infector, agent)

            else:

                combinations = list(itertools.combinations(members, 2))
                for (a, b) in combinations:
                    self.interact(a, b)

# -- Movement --

    def random_spaces(self):

        self.clear_spaces()
        non_isolating = np.argwhere(self.pop.isolation == 0).flatten()
        agents = self.pop.get_agents(non_isolating, active=True)
        self.pop.space[agents] = np.random.randint(self.spaces, size=len(agents))