""" External """
import json

""" Packaging """
__all__ = ['Parameters']

class AbstractParameters:

    def __init__(self, filename=None):

        if filename is not None : self.json_import(filename)

    def json_export(self, filename):

        """ 
        Export an AbstractConfiguration object to a JSON file
        
        Args:
            filename : destination filepath 

        Returns: None
        """

        with open(file=filename, mode='w') as f : json.dump(self.__dict__, f, indent=4)
        
    def json_import(self, filename, complete=False):

        """ 
        Import an AbstractConfiguration object's attributes from an external JSON file

        Args:
            filename : source filepath
            complete : check the source file & Configuation object have matching keys / attributes

        Returns: None
        """

        obj_keys = self.__dict__.keys()
        with open(file=filename, mode='r') as f:

            data = json.load(f)
            json_keys = data.keys()

            for key in json_keys:
                if complete and key not in obj_keys : raise KeyError('Unrecognised key found')
                setattr(self, key, data[key])

            if complete:
                for key in obj_keys:
                    if key not in json_keys : raise KeyError('Unrecognised key found')

class Parameters(AbstractParameters):

    def __init__(self, filename=None):

        """ Create a new Configuration object """

        # General
        self.seed = None
        self.verbose = 0
        self.models = 4
        self.mode = 'serial'
        self.optimisation = True

        self.days = 100
        self.initial = 5
        
        # Population
        self.agents = {
                'student': {'quantity': 5, 'mean': 0.5, 'std': 0},
                'teacher': {'quantity': 3, 'mean': 0.5, 'std': 0.1}
        }

        # Disease
        self.transitions = {
            'initial': {'exposed': 1},
            'exposed': {'symptomatic': 0.995, 'severe': 0.005},
            'severe': {'recovered': 1},
            'symptomatic': {'recovered': 1}

        }
        self.stages = {
            'exposed': {
                'length': {'mean': 5, 'std': 0.4},
                'infectivity': {'mean': 0.02, 'std': 0.001},
                'fatality': {'mean': 0, 'std': 0.001}
            },
            'symptomatic': {
                'length': {'mean': 9, 'std': 0.8},
                'infectivity': {'mean': 0.05, 'std': 0.01},
                'fatality': {'mean': 0.001, 'std': 0}
            },
            'severe': {
                'length': {'mean': 7, 'std': 0.2},
                'infectivity': {'mean': 0.1, 'std': 0.01},
                'fatality': {'mean': 0.06, 'std': 0.1}
            },
            'recovered': {
                'length': {'mean': 20, 'std': 2},
                'infectivity': {'mean': 0.001, 'std': 0},
                'fatality': {'mean': 0, 'std': 0}
            }
        }

        # Interventions
        self.interventions = [
            {'type': 'testing', 'day': 2, 'parameters': {'enabled': True, 'efficiency': 1, 'frequency': 1, 'coverage': 1, 'isolation': 10}},
            {'type': 'vaccination', 'day': 1, 'parameters': {'effect': 0.1}}
        ]

        # Environment
        self.name = 'Placeholder'
        self.path = 'custom.placeholder'
        self.spaces = 3
        self.env_parameters = {'a': 1}

        # Super constructor
        super().__init__(filename=filename)

    def validate(self):

        return True

    def get(self, *vars):
        values = []
        for var in vars:
            if not hasattr(self, var) : raise ValueError(f'attempting to get a member variable {var} which doesn\'t exist')
            values.append(getattr(self, var))
        if len(values) == 0 : return None
        elif len(values) == 1 : return values[0]
        else : return tuple(values)

    def set(self, **vars):
        for var in vars:
            if not hasattr(self, var) : raise ValueError(f'attempting to set a member variable {var} which doesn\'t exist')
            setattr(self, var, vars.get(var))
    
    def get_stage(self, tag):
        if tag not in self.stages.keys() : raise ValueError(f'attempting to get a stage {tag} which doesn\'t exist')
        return self.stages[tag]

    def set_stage(self, tag, length_mean, length_std, infectivity_mean, infectivity_std, fatality_mean, fatality_std):
        if tag not in self.stages.keys() : raise ValueError(f'attempting to set a stage {tag} which doesn\'t exist')
        self.stages[tag] = {
            'length': {'mean': length_mean, 'std': length_std},
            'infectivity': {'mean': infectivity_mean, 'std': infectivity_std},
            'fatality': {'mean': fatality_mean, 'std': fatality_std}
        }

    def get_agent(self, name):
        if name not in self.agents.keys() : raise ValueError(f'attempting to get an agent {name} which doesn\'t exist')
        return self.agents[name]

    def set_agent(self, name, quantity, mean, stdev):
        if name not in self.agents.keys() : raise ValueError(f'attempting to set an agent {name} which doesn\'t exist')
        self.agents[name] = {
            'quantity': quantity,
            'mean': mean,
            'std': stdev
        }

    def add_intervention(self, type, day, **args):
        if self.contains_intervention(type, day) : raise ValueError(f'attempting to add a duplicate of an intervention ({type} - {day})') 
        if type in ['testing', 'vaccination', 'handwashing', 'masks', 'ventilation', 'distancing']:
            self.interventions.append({'type': type, 'day': day, 'parameters': args})
        else:
            raise ValueError('unrecognised intervention type')

    def delete_intervention(self, type, day):
        if not self.contains_intervention(type, day) : raise ValueError(f'attempting to delete an intervention ({type} - {day}) which doesn\'t exist') 
        index = [(i['type'], i['day']) for i in self.interventions].index((type, day))
        self.interventions.pop(index)

    def get_intervention(self, type, day):
        if not self.contains_intervention(type, day) : raise ValueError(f'attempting to get an intervention ({type} - {day}) which doesn\'t exist') 
        index = [(i['type'], i['day']) for i in self.interventions].index((type, day))
        return self.interventions[index]

    def contains_intervention(self, type, day):
        matches = [i for i in self.interventions if i['type'] == type and i['day'] == day]
        return len(matches) > 0