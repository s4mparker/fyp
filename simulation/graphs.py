""" External """
import matplotlib.pyplot as plt
import numpy as np
import time
import copy

""" Internal """

""" Packaging """

class GraphBuilder:

    def __init__(self, ax, results):

        self.options = {
            'Agent States': self.states,
            'Active Case Breakdown': self.active_cases,
            'Total Susceptible': self.total_susceptible,
            'Total Passed': self.total_passed,
            'Total Cases': self.total_cases,
            'Isolation': self.isolation,
            'New Cases': self.new_cases
        }
        self.results = results
        self.selected = list(self.options.keys())[0]
        if ax is None : fig, ax = plt.subplots()
        self.ax = ax

    def get_options(self):

        return list(self.options.keys())

    def set_option(self, value):

        self.selected = value

    def render(self):

        self.ax.clear()
        if self.results is None or self.selected not in list(self.options.keys()): 
            self.ax.text(10, 10, 'Placeholder')
        else: 
            self.options[self.selected](self.ax)

        self.draw_interventions(self.ax)
        self.ax.set_xlim(0)
        self.ax.set_ylim(0)
        
    def draw_interventions(self, ax):

        top = ax.get_ylim()[1]
        interventions = self.results.get_interventions()
        for day in interventions:
            ax.vlines(day, 0, top, label='interventions', colors=['black'], linestyles='dashed')

    def states(self, ax):

        states = copy.deepcopy(self.results.get('states'))
        days = list(states.keys())

        for day in days:
            total = 0
            remove = []
            for key in states[day].keys():
                if key not in ['sus', 'passed']:
                    remove.append(key)
                    total += states[day].get(key)

            states[day]['cases'] = total
            for k in remove : states[day].pop(k)

        keys = []
        for day in states.values():
            for key in day.keys():
                if key not in keys : keys.append(key)
        keys = sorted(keys)

        colours = plt.cm.rainbow(np.linspace(0, 1, len(keys)))

        for i, stage in enumerate(keys):
            c = colours[i]
            points = [day.get(stage, 0) for day in states.values()]
            ax.plot(days, points, c=c, label=stage)

        ax.legend(title='Key')
        ax.set_title('Agent States')
        ax.set_xlabel('Day')
        ax.set_ylabel('State Count (#)')

    def active_cases(self, ax):

        states = copy.deepcopy(self.results.get('states'))
        days = list(states.keys())

        for day in days:
            states[day].pop('sus', None)
            states[day].pop('passed', None)


        keys = []
        for day in states.values():
            for key in day.keys():
                if key not in keys : keys.append(key)
        colours = plt.cm.rainbow(np.linspace(0, 1, len(keys)))

        for i, stage in enumerate(keys):
            c = colours[i]
            points = [day.get(stage, 0) for day in states.values()]
            ax.plot(days, points, c=c, label=stage)

        ax.legend(title='Key')
        ax.set_title('Active Cases')
        ax.set_xlabel('Day')
        ax.set_ylabel('State Count (#)')

    def total_susceptible(self, ax):

        cases = [var.get('sus', 0) for var in copy.deepcopy(self.results.get('states').values())]
        days = range(len(cases))
        ax.plot(days, cases)
        ax.set_title('Total Susceptible')
        ax.set_xlabel('Day')
        ax.set_ylabel('State Count (#)')

    def total_passed(self, ax):

        cases = [var.get('passed', 0) for var in copy.deepcopy(self.results.get('states').values())]
        days = range(len(cases))
        ax.plot(days, cases)
        ax.set_title('Total Passed')
        ax.set_xlabel('Day')
        ax.set_ylabel('State Count (#)')

    def total_cases(self, ax):

        states = copy.deepcopy(self.results.get('states'))
        for day in states.keys():
            total = 0
            for key in states[day].keys():
                if key not in ['sus', 'passed'] : total += states[day].get(key, 0)
            states[day] = total

        ax.plot(states.keys(), states.values())
        ax.set_title('Total Cases')
        ax.set_xlabel('Day')
        ax.set_ylabel('State Count (#)')

    def isolation(self, ax):

        isolating = copy.deepcopy(self.results.get('isolation'))
        for day in isolating.keys():
            isolating[day] = isolating[day].get('isolating', 0)

        ax.plot(isolating.keys(), isolating.values())
        ax.set_title('Isolation')
        ax.set_xlabel('Day')
        ax.set_ylabel('Isolating Count (#)')

    def new_cases(self, ax):

        states = copy.deepcopy(self.results.get('states'))
        cases = []

        for day in states.keys():
            total = 0
            for key in states[day].keys():
                if key not in ['sus', 'passed'] : total += states[day].get(key, 0)
            cases.append(total)

        new_cases = []
        for day in range(len(cases))[1:]:
            new_cases.append(max(cases[day] - cases[day-1], 0))
        new_cases = [0] + new_cases

        ax.plot(range(len(new_cases)), new_cases)
        ax.set_title('New Cases')
        ax.set_xlabel('Day')
        ax.set_ylabel('New Cases (#)')


